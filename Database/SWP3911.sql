USE master
GO
DROP DATABASE IF EXISTS FPT_NIHONGO
GO
CREATE DATABASE FPT_NIHONGO
GO
USE FPT_NIHONGO
GO
CREATE TABLE [role]
(
    role_id   INT PRIMARY KEY,
    role_name varchar(50)
)
GO
INSERT INTO [role] (role_id, role_name)
VALUES (1, 'Admin');
INSERT INTO [role] (role_id, role_name)
VALUES (2, 'Instructor');
INSERT INTO [role] (role_id, role_name)
VALUES (3, 'Student');
GO

CREATE TABLE admin
(
    admin_id INTEGER IDENTITY (1,1) PRIMARY KEY,
    email    VARCHAR(50) UNIQUE NOT NULL,
    active   BIT DEFAULT 1,
    password VARCHAR(100)       NOT NULL,
    role_id  INT                not null,
    FOREIGN KEY (role_id) REFERENCES [role] (role_id)
)
GO
-- Password: admin
INSERT INTO admin (email, password, role_id)
VALUES ('anhquoc@gmail.com', '$2a$10$rUsfEbbi3rtglIUMxZhhoOy77xmuCyuX4qKNwB8b3FO6sYXiWMMJe', 1);
GO
CREATE TABLE instructor
(
    instructor_id  INTEGER IDENTITY (1,1) PRIMARY KEY,
    active         BIT DEFAULT 1,
    password       VARCHAR(100) NOT NULL,
    full_name      VARCHAR(100) NOT NULL,
    specialization VARCHAR(100),
    email          VARCHAR(100) NOT NULL,
    role_id        INT          not null,
    FOREIGN KEY (role_id) REFERENCES [role] (role_id)
)
GO
CREATE TABLE student
(
    student_id      INTEGER IDENTITY (1,1) PRIMARY KEY,
    email           VARCHAR(100) UNIQUE NOT NULL,
    email_verified  BIT                DEFAULT 0,
    phone_number    VARCHAR(10)        DEFAULT 0,
    active          BIT                DEFAULT 1,
    password        VARCHAR(100),
    full_name       NVARCHAR(100)        NOT NULL,
    date_of_birth   DATE,
    picture_Url     VARCHAR(255),
    family_name     NVARCHAR(100),
    given_name      NVARCHAR(100),
    enrollment_date DATE,
    role_id         INT                 not null,
    FOREIGN KEY (role_id) REFERENCES [role] (role_id)
)
GO
CREATE TABLE transaction_types
(
    id   INTEGER IDENTITY (1,1) PRIMARY KEY,
    name NVARCHAR(30),
)
GO
INSERT INTO transaction_types
VALUES ('Deposit'),
       ('Expense')
GO
CREATE TABLE student_transactions
(
    id                  INTEGER IDENTITY (1,1) PRIMARY KEY,
    student_id          INTEGER FOREIGN KEY REFERENCES student (student_id),
    form_of_payment     NVARCHAR(30)  NOT NULL,
    total_price         DECIMAL(5, 2) NOT NULL DEFAULT 0,
    transaction_type_id INTEGER FOREIGN KEY REFERENCES transaction_types (id),
    trade_date          DATETIME               DEFAULT CURRENT_TIMESTAMP,
)
GO
CREATE TABLE category_course
(
    category_id   INT PRIMARY KEY,
    category_name VARCHAR(50) NOT NULL
)
GO
CREATE TABLE course
(
    course_id     INT Identity (1,1) PRIMARY KEY,
    course_name   VARCHAR(50)  NOT NULL,
    img_URL       VARCHAR(255) NOT NULL,
    start_date    DATE         NOT NULL,
    end_date      DATE         NOT NULL,
    description   VARCHAR(250) NOT NULL,
    category_id   INT          NOT NULL,
    instructor_id INT          NOT NULL,
    active        BIT          NOT NULL,
    price         FLOAT        NOT NULL,
    discount      FLOAT        NOT NULL,
    link_meet     VARCHAR(255) NOT NULL,
    FOREIGN KEY (category_id) REFERENCES category_course (category_id),
    FOREIGN KEY (instructor_id) REFERENCES instructor (instructor_id)
)
GO
CREATE TABLE lecture
(
    lecture_id   INT IDENTITY (1,1) PRIMARY KEY,
    lecture_name VARCHAR(50)  NOT NULL,
    description  VARCHAR(250) NOT NULL,
    time         INT          NOT NULL,
    image_url    VARCHAR(255) NOT NULL,
    video_url    VARCHAR(255) NOT NULL,
    course_id    INT          NOT NULL,
    FOREIGN KEY (course_id) REFERENCES course (course_id)
)
GO
CREATE TABLE exam
(
    exam_id     INT PRIMARY KEY,
    exam_name   VARCHAR(50)  NOT NULL,
    description VARCHAR(250) NOT NULL,
    start_date  DATE         NOT NULL,
    end_date    DATE         NOT NULL,
    course_id   INT          NOT NULL,
    FOREIGN KEY (course_id) REFERENCES course (course_id)
)
GO
CREATE TABLE grade
(
    grade_id   INT PRIMARY KEY,
    student_id INT            NOT NULL,
    exam_id    INT            NOT NULL,
    score      DECIMAL(10, 2) NOT NULL,
    FOREIGN KEY (student_id) REFERENCES student (student_id),
    FOREIGN KEY (exam_id) REFERENCES exam (exam_id)
)
GO
CREATE TABLE order_table
(
    order_id   INT PRIMARY KEY,
    student_id INT            NOT NULL,
    order_date DATE           NOT NULL,
    total      DECIMAL(10, 2) NOT NULL,
    status     VARCHAR(50)    NOT NULL,
    notes      VARCHAR(50)    NOT NULL,
    FOREIGN KEY (student_id) REFERENCES student (student_id)
)
GO
CREATE TABLE order_detail
(
    detail_id INT PRIMARY KEY,
    order_id  INT            NOT NULL,
    course_id INT            NOT NULL,
    price     DECIMAL(10, 2) NOT NULL,
    quantity  INT            NOT NULL,
    FOREIGN KEY (order_id) REFERENCES order_table (order_id),
    FOREIGN KEY (course_id) REFERENCES course (course_id)
)
GO
CREATE TABLE enrollment
(
    enrollment_id   INT PRIMARY KEY,
    student_id      INT  NOT NULL,
    course_id       INT  NOT NULL,
    enrollment_date DATE NOT NULL,
    FOREIGN KEY (student_id) REFERENCES student (student_id),
    FOREIGN KEY (course_id) REFERENCES course (course_id)
)
GO
CREATE TABLE category_blog
(
    category_id   INT PRIMARY KEY,
    category_name VARCHAR(50) NOT NULL
)
GO
CREATE TABLE blog
(
    blog_id      INT PRIMARY KEY,
    blog_details varchar(255) not null,
    img_URL      varchar(255) not null,
    title        VARCHAR(100) NOT NULL,
    content      TEXT         NOT NULL,
    publish_date DATE         NOT NULL,
    admin_id     INT          NOT NULL,
    category_id  INT          NOT NULL,
    FOREIGN KEY (admin_id) REFERENCES admin (admin_id),
    FOREIGN KEY (category_id) REFERENCES category_blog (category_id)
)
GO

--------------------------------- insert data -----------------------------

Insert into category_course
values (1, 'IT & Software'),
       (2, 'Photography'),
       (3, 'Programing Language'),
       (4, 'Language')
GO
Insert into instructor
values (1, '123', 'John Marr', 'Associate professor with PhD in information technology at the university',
        'abc1@gmail.com', 2),
       (1, '123', 'Jake Smith', 'Lecturer in linguistics at the Cambridge University', 'abc2@gmail.com', 2),
       (1, '123', 'Thomas Williams', 'Lecturer at University of Technology', 'abc3@gmail.com', 2),
       (1, '123', 'Emma Wilson', 'Professor specializing in graphic design at the university', 'abc4@gmail.com', 2)
GO
Insert into course
values ('HTML', 'assets/images/courses/pic1.jpg', '2024-04-13', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        3, 3, 1, 120, 0.2, ''),
       ('CSS', 'assets/images/courses/pic2.jpg', '2024-04-14', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        3, 3, 1, 120, 0.2, ''),
       ('Javascript', 'assets/images/courses/pic3.jpg', '2024-04-15', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        3, 3, 1, 120, 0.2, ''),
       ('C#', 'assets/images/courses/pic4.jpg', '2024-04-16', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        3, 3, 1, 120, 0.2, ''),
       ('.NET', 'assets/images/courses/pic1.jpg', '2024-04-17', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        3, 3, 1, 120, 0.2, ''),
       ('Springboot', 'assets/images/courses/pic6.jpg', '2024-04-18', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        3, 3, 1, 120, 0.2, ''),
       ('Software engineering', 'assets/images/courses/pic7.jpg', '2024-04-19', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        1, 1, 1, 120, 0.2, ''),
       ('Programing with Unity', 'assets/images/courses/pic8.jpg', '2024-04-20', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        3, 3, 1, 120, 0.2, ''),
       ('Japanese N1', 'assets/images/courses/pic9.jpg', '2024-04-21', '2024-6-20',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        4, 2, 1, 120, 0.2, ''),
       ('Japanese N2', 'assets/images/courses/pic1.jpg', '2024-04-22', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        4, 2, 1, 120, 0.2, ''),
       ('Japanese N3', 'assets/images/courses/pic2.jpg', '2024-04-23', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        4, 2, 1, 120, 0.2, ''),
       ('Japanese N4', 'assets/images/courses/pic3.jpg', '2024-04-24', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        4, 2, 1, 120, 0.2, ''),
       ('Japanese N5', 'assets/images/courses/pic4.jpg', '2024-04-25', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        4, 2, 1, 120, 0.2, ''),
       ('Agile Development', 'assets/images/courses/pic1.jpg', '2024-04-26', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        1, 1, 1, 120, 0.2, ''),
       ('Art and Design', 'assets/images/courses/pic6.jpg', '2024-04-27', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        2, 4, 1, 120, 0.2, ''),
       ('Camera Control', 'assets/images/courses/pic7.jpg', '2024-04-28', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        2, 4, 1, 120, 0.2, ''),
       ('Fundamentals of Digital Image and Video Processing', 'assets/images/courses/pic8.jpg', '2024-04-29', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        2, 4, 1, 120, 0.2, ''),
       ('Graphic Design', 'assets/images/courses/pic9.jpg', '2024-04-30', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        2, 4, 1, 120, 0.2, ''),
       ('Game Design and Development', 'assets/images/courses/pic1.jpg', '2024-05-01', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        2, 4, 1, 120, 0.2, ''),
       ('Cybersecurity', 'assets/images/courses/pic2.jpg', '2024-05-02', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        1, 1, 1, 120, 0.2, ''),
       ('Artificial intelligence', 'assets/images/courses/pic3.jpg', '2024-05-03', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        1, 1, 1, 120, 0.2, ''),
       ('Computer Engineering', 'assets/images/courses/pic4.jpg', '2024-05-04', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        1, 1, 1, 120, 0.2, ''),
       ('Applications development', 'assets/images/courses/pic1.jpg', '2024-05-06', '2024-6-13',
        'HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.',
        1, 1, 1, 120, 0.2, '')
GO
INSERT INTO lecture (lecture_name, description, time, image_url, video_url, course_id)
VALUES ('Lecture 1', 'Description 1', 60, 'image1.jpg', 'video1.mp4', 1);
GO
INSERT INTO lecture (lecture_name, description, time, image_url, video_url, course_id)
VALUES ('Lecture 2', 'Description 2', 45, 'image2.jpg', 'video2.mp4', 1);
GO
INSERT INTO lecture (lecture_name, description, time, image_url, video_url, course_id)
VALUES ('Lecture 2. Introduction', 'Description 1', 60, 'image1.jpg', 'video1.mp4', 2);
GO
INSERT INTO lecture (lecture_name, description, time, image_url, video_url, course_id)
VALUES ('Lecture 2. CSS', 'Description 2', 45, 'image2.jpg', 'video2.mp4', 2);
GO

-- Thêm dữ liệu vào bảng student
INSERT INTO student (email, email_verified, phone_number, active, password, full_name, date_of_birth, picture_Url, family_name, given_name, enrollment_date, role_id)
VALUES
    ('student1@example.com', 0, '1234567890', 1, 'password1', 'John Doe', '2000-01-01', 'http://example.com/pictures/john_doe.jpg',  'Doe', 'John', '2022-01-15', 1),
    ('student2@example.com', 0, '2345678901', 1, 'password2', 'Jane Smith', '2001-02-03', 'http://example.com/pictures/jane_smith.jpg', 'Smith', 'Jane', '2022-02-20', 2),
    ('student3@example.com', 0, '3456789012', 1, 'password3', 'Michael Johnson', '1999-05-10', 'http://example.com/pictures/michael_johnson.jpg', 'Johnson', 'Michael', '2022-03-10', 1)

GO
insert into category_blog
values (1, 'Khanh')
GO
INSERT INTO [dbo].[blog]
( [blog_id]
, [blog_details]
, [title]
, [content]
, [publish_date]
, [admin_id]
, [category_id]
, [img_URL])
VALUES ( 1
       , 'Blog details 1'
       , 'Title 1'
       , 'Content 1'
       , '2024-05-22'
       , 1
       , 1
       , 'image1.jpg')
GO
INSERT INTO [dbo].[blog]
( [blog_id]
, [blog_details]
, [title]
, [content]
, [publish_date]
, [admin_id]
, [category_id]
, [img_URL])
VALUES ( 2
       , 'Blog details 1'
       , 'Title 2'
       , 'Content 2'
       , '2024-05-22'
       , 1
       , 1
       , 'image1.jpg')
GO
INSERT INTO [dbo].[blog]
( [blog_id]
, [blog_details]
, [title]
, [content]
, [publish_date]
, [admin_id]
, [category_id]
, [img_URL])
VALUES ( 3
       , 'Blog details 3'
       , 'Title 3'
       , 'Content 3'
       , '2024-05-22'
       , 1
       , 1
       , 'image1.jpg')
