<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
    </head>
    <body id="bg">
        <div class="page-wraper">
            <div id="loading-icon-bx"></div>
            <!-- Header Top ==== -->
            <header class="header rs-nav header-transparent">
                <div class="top-bar">
                    <div class="container">
                        <div class="row d-flex justify-content-between">
                            <div class="topbar-left">
                                <ul>
                                    <li><a href="#"><i class="fa fa-question-circle"></i>Ask a Question</a></li>
                                    <li><a href="#"><i class="fa fa-envelope-o"></i>Support@website.com</a></li>
                                </ul>
                            </div>
                            <div class="topbar-right">
                                <ul>
                                    <li>
                                        <select class="header-lang-bx">
                                            <option data-icon="flag flag-uk">English UK</option>
                                            <option data-icon="flag flag-us">English US</option>
                                        </select>
                                    </li>
                                    <li><a href="login">Login</a></li>
                                    <li><a href="signup">Register</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sticky-header navbar-expand-lg">
                    <div class="menu-bar clearfix">
                        <div class="container clearfix">
                            <!-- Header Logo ==== -->
                            <div class="menu-logo"> 
                                <c:forEach var="o" items="${listHeader}">
                                    <c:if test="${o.id.equals('logo')}">
                                        <a href="home">
                                            <img src="<c:out value="${o.description}" />" alt="logo">
                                        </a>    
                                    </c:if>
                                </c:forEach>   
                            </div>


                            <!-- Mobile Nav Button ==== -->
                            <button class="navbar-toggler collapsed menuicon justify-content-end" type="button" data-toggle="collapse" data-target="#menuDropdown" aria-controls="menuDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                            <!-- Author Nav ==== -->
                            <div class="secondary-menu">
                                <div class="secondary-inner">
                                    <ul>
                                        <li><a href="javascript:;" class="btn-link"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="javascript:;" class="btn-link"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="javascript:;" class="btn-link"><i class="fa fa-linkedin"></i></a></li>
                                        <!-- Search Button ==== -->
                                        <li class="search-btn"><button id="quik-search-btn" type="button" class="btn-link"><i class="fa fa-search"></i></button></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Search Box ==== -->
                            <div class="nav-search-bar">
                                <form action="courses" method="post">
                                    <input name="name" value="" type="text" class="form-control" placeholder="Type to search">
                                    <span><button class="btn" type="submit"><i class="ti-search"></i></button></span>
                                </form>
                                <span id="search-remove"><i class="ti-close"></i></span>
                            </div>
                            <!-- Navigation Menu ==== -->
                            <div class="menu-links navbar-collapse collapse justify-content-start" id="menuDropdown">
                                <div class="menu-logo"> 
                                    <c:forEach var="o" items="${listHeader}">
                                        <c:if test="${o.id == '1'}">
                                            <a href="home">
                                                <img src="<c:out value="${o.description}" />" alt="logo">
                                            </a>    
                                        </c:if>
                                    </c:forEach>   
                                </div>

                                <ul class="nav navbar-nav">	
                                    <c:forEach var="o" items="${listHeader}">
                                        <c:if test="${o.id.equals('main_menu_1')}">
                                            <c:url var="homeUrl" value="home" />
                                            <li >
                                                <a href="<c:out value="${homeUrl}" />"><c:out value="${o.description}" /></a>
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                            
                                    <c:forEach var="o" items="${listHeader}">
                                        <c:if test="${o.id == '3'}">
                                            <c:url var="homeUrl" value="#" />
                                            <li >
                                                <a href="<c:out value="${homeUrl}" />"><c:out value="${o.description}" /></a>
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                    <c:forEach var="o" items="${listHeader}">
                                        <c:if test="${o.id == '4'}">
                                            <c:url var="homeUrl" value="courses" />
                                            <li >
                                                <a href="<c:out value="${homeUrl}" />"><c:out value="${o.description}" /></a>
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                            
                                    <c:forEach var="o" items="${listHeader}">
                                        <c:if test="${o.id == '5'}">
                                            <c:url var="homeUrl" value="categoryblog" />
                                            <li >
                                                <a href="<c:out value="${homeUrl}" />"><c:out value="${o.description}" /></a>
                                            </li>
                                        </c:if>
                                    </c:forEach>


                                    <!--                                    <li><a href="javascript:;">Blog <i class="fa fa-chevron-down"></i></a>
                                                                            <ul class="sub-menu">
                                                                                <li><a href="blog-classic-grid.html">Blog Classic</a></li>
                                                                                <li><a href="blog-classic-sidebar.html">Blog Classic Sidebar</a></li>
                                                                                <li><a href="blog-list-sidebar.html">Blog List Sidebar</a></li>
                                                                                <li><a href="blog-standard-sidebar.html">Blog Standard Sidebar</a></li>
                                                                                <li><a href="blog-details.html">Blog Details</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li class="nav-dashboard"><a href="javascript:;">Dashboard <i class="fa fa-chevron-down"></i></a>
                                                                            <ul class="sub-menu">
                                                                                <li><a href="admin/index.html">Dashboard</a></li>
                                                                                <li><a href="admin/add-listing.html">Add Listing</a></li>
                                                                                <li><a href="admin/bookmark.html">Bookmark</a></li>
                                                                                <li><a href="admin/courses.html">Courses</a></li>
                                                                                <li><a href="admin/review.html">Review</a></li>
                                                                                <li><a href="admin/teacher-profile.html">Teacher Profile</a></li>
                                                                                <li><a href="admin/user-profile.html">User Profile</a></li>
                                                                                <li><a href="javascript:;">Calendar<i class="fa fa-angle-right"></i></a>
                                                                                    <ul class="sub-menu">
                                                                                        <li><a href="admin/basic-calendar.html">Basic Calendar</a></li>
                                                                                        <li><a href="admin/list-view-calendar.html">List View Calendar</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li><a href="javascript:;">Mailbox<i class="fa fa-angle-right"></i></a>
                                                                                    <ul class="sub-menu">
                                                                                        <li><a href="admin/mailbox.html">Mailbox</a></li>
                                                                                        <li><a href="admin/mailbox-compose.html">Compose</a></li>
                                                                                        <li><a href="admin/mailbox-read.html">Mail Read</a></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </li>-->
                                </ul>
                                <div class="nav-social-link">
                                    <a href="javascript:;"><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:;"><i class="fa fa-google-plus"></i></a>
                                    <a href="javascript:;"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                            <!-- Navigation Menu END ==== -->
                        </div>
                    </div>
                </div>
            </header>
            <!-- Header Top END ==== -->


        </div>

        <!-- External JavaScripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assets/vendors/counter/waypoints-min.js"></script>
        <script src="assets/vendors/counter/counterup.min.js"></script>
        <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assets/vendors/masonry/masonry.js"></script>
        <script src="assets/vendors/masonry/filter.js"></script>
        <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
        <script src="assets/js/functions.js"></script>
        <script src="assets/js/contact.js"></script>
        <script src='assets/vendors/switcher/switcher.js'></script>
        <!-- Revolution JavaScripts Files -->
        <script src="assets/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="assets/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- Slider revolution 5.0 Extensions  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script>
            jQuery(document).ready(function () {
                var ttrevapi;
                var tpj = jQuery;
                if (tpj("#rev_slider_486_1").revolution == undefined) {
                    revslider_showDoubleJqueryError("#rev_slider_486_1");
                } else {
                    ttrevapi = tpj("#rev_slider_486_1").show().revolution({
                        sliderType: "standard",
                        jsFileLocation: "assets/vendors/revolution/js/",
                        sliderLayout: "fullwidth",
                        dottedOverlay: "none",
                        delay: 9000,
                        navigation: {
                            keyboardNavigation: "on",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "on",
                            touch: {
                                touchenabled: "on",
                                swipe_threshold: 75,
                                swipe_min_touches: 1,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            }
                            ,
                            arrows: {
                                style: "uranus",
                                enable: true,
                                hide_onmobile: false,
                                hide_onleave: false,
                                tmp: '',
                                left: {
                                    h_align: "left",
                                    v_align: "center",
                                    h_offset: 10,
                                    v_offset: 0
                                },
                                right: {h_align: "right",
                                    v_align: "center",
                                    h_offset: 10,
                                    v_offset: 0
                                }
                            },

                        },
                        viewPort: {
                            enable: true,
                            outof: "pause",
                            visible_area: "80%",
                            presize: false
                        },
                        responsiveLevels: [1240, 1024, 778, 480],
                        visibilityLevels: [1240, 1024, 778, 480],
                        gridwidth: [1240, 1024, 778, 480],
                        gridheight: [768, 600, 600, 600],
                        lazyType: "none",
                        parallax: {
                            type: "scroll",
                            origo: "enterpoint",
                            speed: 400,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 46, 47, 48, 49, 50, 55],
                            type: "scroll",
                        },
                        shadow: 0,
                        spinner: "off",
                        stopLoop: "off", stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        hideThumbsOnMobile: "off", hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }
            });
        </script>
    </body>

</html>
