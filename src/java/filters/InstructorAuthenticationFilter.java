package filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import services.JwtTokenProvider;

import java.io.IOException;

@WebFilter(filterName = "ManagerAuthenticationFilter",
        urlPatterns = {""},
        dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD})
public class InstructorAuthenticationFilter implements Filter {

    private JwtTokenProvider jwtTokenProvider;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        jwtTokenProvider = new JwtTokenProvider();
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // Check cookie has session ID or not
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        Cookie authenticationCookie = getAuthenticationCookie(req);
        if(authenticationCookie != null) {
            if(isCookieValid(authenticationCookie)){
                if(isRoleInstructor(authenticationCookie)) {
                    servletRequest.setAttribute("instructor_id", getInstructorId(authenticationCookie));
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            } else {
                Cookie newCookie = new Cookie("instructor_token", "");
                newCookie.setMaxAge(0);
                newCookie.setDomain("localhost");
                newCookie.setPath("/FPT-Nihongo");
                newCookie.setHttpOnly(true);
                res.addCookie(newCookie);
            }
        }
        res.sendRedirect("login");
    }
    
    private int getInstructorId(Cookie cookie){
        return jwtTokenProvider.getUserIdFromJWT(cookie.getValue());
    }

    private boolean isCookieValid(Cookie cookie){
        return jwtTokenProvider.validateToken(cookie.getValue());
    }

    private boolean isRoleInstructor(Cookie cookie){
        return jwtTokenProvider.getRoleFromJWT(cookie.getValue()).equals("2");
    }

    private Cookie getAuthenticationCookie(HttpServletRequest req){
        Cookie[] cookies = req.getCookies();
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("instructor_token")) {
                    return cookie;
                }
            }
        }
        return null;
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
