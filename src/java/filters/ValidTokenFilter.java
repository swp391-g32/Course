package filters;

import entities.Student;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import services.StudentService;
import services.TokenService;

import java.io.IOException;

@WebFilter(filterName = "ValidTokenFilter",
        urlPatterns = {"/reset-password"},
        dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD})
public class ValidTokenFilter implements Filter {

    private TokenService tokenService;
    private StudentService studentService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        tokenService = new TokenService();
        studentService = StudentService.getInstance();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String token = servletRequest.getParameter("token");

        if(tokenService.validateToken(token)) {

            String email = tokenService.getActorEmail(token);
            Student student = studentService.getStudentByEmail(email);

            if(student == null) {
                servletRequest.setAttribute("message", "Invalid token.");
                servletRequest.getRequestDispatcher("forget-password.jsp").forward(servletRequest, servletResponse);
            }
            servletRequest.setAttribute("student", student);
            if(student.getPassword() != null){
                String oldPassword = tokenService.getOldPassword(token);
                if(oldPassword != null){
                    if(student.getPassword().substring(10, 30).equals(oldPassword)){
                        filterChain.doFilter(servletRequest, servletResponse);
                    } else {
                        servletRequest.setAttribute("message", "Invalid token.");
                        servletRequest.getRequestDispatcher("forget-password.jsp").forward(servletRequest, servletResponse);
                    }
                } else {
                    servletRequest.setAttribute("message", "Invalid token.");
                    servletRequest.getRequestDispatcher("forget-password.jsp").forward(servletRequest, servletResponse);
                }
            } else {
                servletRequest.setAttribute("token", token);
                filterChain.doFilter(servletRequest, servletResponse);
            }

        }




    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
