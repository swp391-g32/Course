package services;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import entities.IActor;
import io.jsonwebtoken.*;

import java.util.Base64;
import java.util.Date;

public class JwtTokenProvider {
    private final String JWT_SECRET = "U2FsdGVkX1/1nMZ54TUV7X9sdcY79Pi29MptWQ2xwQ0=";

    private final long JWT_EXPIRATION = 86400000L; // 1 day in milliseconds

    public String generateToken(IActor iActor) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + JWT_EXPIRATION);

        // Create a Claims object and set the necessary claims
        Claims claims = Jwts.claims();
        claims.setSubject(iActor.getId().toString());
        claims.put("role", iActor.getRoleId().toString());
        claims.setIssuedAt(now);
        claims.setExpiration(expiryDate);

        // Build and return the JWT
        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                .compact();
    }

    public int getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();
        
        return Integer.parseInt(claims.getSubject());
    }

    public JsonObject getPayload(String token) {
        // Create a Gson instance
        Gson gson = new Gson();

        try {
            // Decode the payload of the JWT
            String[] chunks = token.split("\\.");
            Base64.Decoder decoder = Base64.getUrlDecoder();
            String payload = new String(decoder.decode(chunks[1]));

            // Convert the decoded payload to JsonObject
            JsonObject decodedPayloadJson = gson.fromJson(payload, JsonObject.class);
            return decodedPayloadJson;
        } catch (JsonSyntaxException e) {
            System.out.println("Invalid JSON string: " + e.getMessage());
        }
        return null;
    }

    public String getRoleFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();

        return (String) claims.get("role");
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException ex) {
            System.out.println("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            System.out.println("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            System.out.println("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            System.out.println("JWT claims string is empty.");
        }
        return false;
    }
}