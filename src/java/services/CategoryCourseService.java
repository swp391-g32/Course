/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package services;

import daos.CategoryCourseDAO;
import entities.CategoryCourse;
import java.util.List;

/**
 *
 * @author FPT SHOP
 */
public class CategoryCourseService {

    private static CategoryCourseService instance;

    private CategoryCourseDAO categoryCourseDAO;

    public CategoryCourseService() {
        categoryCourseDAO = new CategoryCourseDAO();
    }

    public static CategoryCourseService getInstance() {
        if (instance == null) {
            instance = new CategoryCourseService();
        }
        return instance;
    }

    public List<CategoryCourse> getAllCategoryCourse() {
        List<CategoryCourse> result = categoryCourseDAO.getAllCategoryCourse();
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result;
    }

    public CategoryCourse getCategoryCourseById(int id) {
        return categoryCourseDAO.getCategoryCourseById(id);
    }

    public CategoryCourse getCategoryCourseByCourseId(String courseIDString) {
        int courseID;
        if (courseIDString == null || courseIDString.isBlank()) {
            courseID = 0;
        } else {
            courseID = Integer.parseInt(courseIDString);
        }

        CategoryCourse categoryCourse = categoryCourseDAO.getCategoryCourseByCourseId(courseID);
        if (categoryCourse == null) {
            // Xử lý khi không tìm thấy categoryCourse
            categoryCourse = new CategoryCourse();
            categoryCourse.setName("Unknown");
            // Các thuộc tính khác của categoryCourse cũng có thể được set ở đây
        }

        return categoryCourse;
    }
}
