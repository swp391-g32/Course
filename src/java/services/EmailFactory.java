package services;

public class EmailFactory {
    private static EmailFactory instance;
    private EmailService emailService;

    private final String OTP_URL_TEMPLATE = "http://localhost:8080/FPT-Nihongo/reset-password?token=";

    private EmailFactory() {
        emailService = EmailService.getInstance();
    }

    public static EmailFactory getInstance() {
        if (instance == null) {
            instance = new EmailFactory();
        }
        return instance;
    }

    public String generateResetPasswordEmail(String token) {
        String otpLink = OTP_URL_TEMPLATE + token;
        return "<html>" +
                "<body style=\"font-family: Arial, sans-serif; color: #333;\">" +
                "<div style=\"max-width: 600px; margin: 0 auto; padding: 20px; border: 1px solid #e0e0e0; border-radius: 5px;\">" +
                "<h2 style=\"color: #4CAF50;\">Password Reset Request</h2>" +
                "<p>Dear User,</p>" +
                "<p>We received a request to reset your password. Click the link below to choose a new password:</p>" +
                "<p style=\"text-align: center;\"><a href=\"" + otpLink + "\" style=\"background-color: #4CAF50; color: white; padding: 10px 20px; text-decoration: none; border-radius: 5px;\">Reset Password</a></p>" +
                "<p>If you did not request a password reset, please ignore this email. This link will expire in 15 minute for your security.</p>" +
                "<p>Best regards,<br>FPT Nihongo Team</p>" +
                "</div>" +
                "</body>" +
                "</html>";
    }
}
