package services;

import daos.InstructorDAO;
import entities.Instructor;
import org.mindrot.jbcrypt.BCrypt;
import java.util.List;

public class InstructorService {

    private static InstructorService instance;
    private InstructorDAO instructorDAO;
    private InstructorService(){
        instructorDAO = new InstructorDAO();
    }

    public static InstructorService getInstance(){
        if(instance == null){
            instance = new InstructorService();
        }
        return instance;
    }

    public List<Instructor> getAllInstructor(){
        List<Instructor> result = instructorDAO.getAllInstructor();
        if(result == null || result.isEmpty()){
            return null;
        }
        return result;
    }

    public Instructor getInstructorById(int id){
        return instructorDAO.getInstructorById(id);
    }
    
    public Instructor getInstructorByCourseId(String courseIDString) {
    int courseID;
    if (courseIDString == null || courseIDString.isBlank()) {
        courseID = 0;
    } else {
        courseID = Integer.parseInt(courseIDString);
    }
    
    Instructor instructor = instructorDAO.getInstructorByCourseId(courseID);
    if (instructor == null) {
        // Xử lý khi không tìm thấy người hướng dẫn
        return null; // hoặc trả về một giá trị mặc định khác tùy thuộc vào yêu cầu
    }
    
    return instructor;
}

    public Instructor getAccountByEmailAndPassword(String username, String password) {
        Instructor instructor = instructorDAO.findByEmail(username);
        if(instructor == null){
            return null;
        }
        if(!BCrypt.checkpw(password, instructor.getPassword())){
            return null;
        }
        return instructor;
    }
   

}