/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package services;

import daos.LectureDAO;
import entities.Lecture;
import java.util.List;

public class LectureService {

    private static LectureService instance;

    private LectureDAO lectureDAO;

    public LectureService() {
        lectureDAO = new LectureDAO();
    }

    public static LectureService getInstance() {
        if (instance == null) {
            instance = new LectureService();
        }
        return instance;
    }

    public List<Lecture> getAllLectureByCourseId(String courseIDString) {
        int courseID;
        if (courseIDString == null || courseIDString.isBlank()) {
            courseID = 0;
        } else {
            try {
                courseID = Integer.parseInt(courseIDString);
            } catch (NumberFormatException e) {
                courseID = 0;
            }
        }
        List<Lecture> result = lectureDAO.getAllLectureByCourseId(courseID);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result;
    }
    
    public Lecture getLectureById(String idString) {
        int id = returnValidID(idString);
        if(id > lectureDAO.getLectureMaxID().getId() && id < lectureDAO.getLectureMinID().getId()){
            return null;
        }else{
            return lectureDAO.getLectureById(id);
        }
    }

    public int countAllLectureByCourseId(String courseIDString) {
        int courseID;
        if (courseIDString == null || courseIDString.isBlank()) {
            courseID = 0;
        } else {
            courseID = Integer.parseInt(courseIDString);
        }
        int result = lectureDAO.countAllLectureByCourseId(courseID);
        return result;
    }

    public int getTotalCourseHours(String courseIDString) {
        int courseID;
        if (courseIDString == null || courseIDString.isBlank()) {
            courseID = 0;
        } else {
            courseID = Integer.parseInt(courseIDString);
        }
        int totalMinutes = lectureDAO.getTotalCourseHours(courseID);
//        if (totalMinutes == null) {
//            return 0;
//        }
        int totalHours = totalMinutes / 60;
        return totalHours;
    }

    public Lecture insertNewLecture(String lecture_name, String description, String timeString, String image_url, String video_url, String course_id) {
        int courseID;
        if (course_id == null || course_id.isBlank()) {
            courseID = 0;
        } else {
            courseID = Integer.parseInt(course_id);
        }
        int time;
        if (timeString == null || timeString.isBlank()) {
            time = 0;
        } else {
            time = Integer.parseInt(timeString);
        }
        Lecture lecture = new Lecture(0, lecture_name, time, description, image_url, video_url, courseID);
        lectureDAO.insertLecture(lecture);
        return lecture;
    }
    
     public Lecture updateLecture(String name,String timeString,String description, String img,String videoString,String idString ){
        int id = returnValidID(idString);
        int time=returnValidID(timeString);
        Lecture lecture = new Lecture(id, name, time, description, img, videoString);
        lectureDAO.updateLecture(lecture);
        return lecture;
    }   

    public String sendErrorAddTitle(String titleString) {
        if (titleString == null || titleString.isBlank()) {
            return "This field need to be fill";
        } else {
            return null;
        }
    }

    public String sendErrorAddTime(String priceString) {
        if (priceString == null || priceString.isBlank()) {
            return "This field need to be fill";
        }
        try {
            double price = Double.parseDouble(priceString);

            if (price <= 0) {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException | NullPointerException e) {
            return "The price must be positive number!";
        }
        return null;
    }

    public static void main(String[] args) {
        LectureService dao = new LectureService();

        int totalTimes = dao.getTotalCourseHours("2");
        System.out.println(totalTimes);
    }

    public List<Lecture> getNextCourse(String page, int numberOfLectureInPage) {
        int pageNumber;
        if (page == null) {
            pageNumber = 1;
        } else {
            pageNumber = Integer.parseInt(page);
        }
        List<Lecture> result = lectureDAO.getNextLecture(pageNumber, numberOfLectureInPage);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result;
    }

    public int returnValidPageNumber(String pageString) {
        try {
            int page = Integer.parseInt(pageString);

            if (page <= 0 || pageString.isEmpty() || pageString.isBlank()) {
                throw new NumberFormatException();
            }
            return page;
        } catch (NumberFormatException | NullPointerException e) {
            return 1;
        }
    }

    public List<Lecture> getNextLectureAfterFilterAll(String keyName, int sort, String pageString, int numberOfLectureInPage) {

        int page = returnValidPageNumber(pageString);

        List<Lecture> result = lectureDAO.getNextLectureAfterFilterAll(keyName, sort, page, numberOfLectureInPage);
        return result;
    }

    public int getNumberOfLectureAfterFilterAll(String keyName) {
    
        return (int)lectureDAO.getNumberOfLectureAfterFilterAll(keyName);
    }

    public int getNumberOfPageAfterFilterAll(String keyName, int numberOfLecture) {

        return (int) Math.ceil(lectureDAO.getNumberOfLectureAfterFilterAll(keyName) * 1.0 / numberOfLecture);
    }
    
    public int returnValidID(String iDString) {
        try {
            int id = Integer.parseInt(iDString);

            if (id <= 0 || iDString.isEmpty() || iDString.isBlank()) {
                throw new NumberFormatException();
            }
            return id;
        } catch (NumberFormatException | NullPointerException e) {
            return 0;
        }
    }
    
     public void deleteLecture(String iDString){
        int id = returnValidID(iDString);
        lectureDAO.deleteLectureById(id);
    }
}
