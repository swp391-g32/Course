package services;

import com.google.api.client.util.DateTime;
import daos.StudentDAO;
import entities.Student;
import java.util.List;
import org.mindrot.jbcrypt.BCrypt;

import java.util.regex.Pattern;
import mappers.impl.StudentMapper;

public class StudentService {

    private final String FIND_PHONE = "select * from student where phone_number = ?";
    private static StudentService instance;

    private StudentDAO studentDAO;

    private StudentService() {
        studentDAO = new StudentDAO();
    }

    public static StudentService getInstance() {
        if (instance == null) {
            instance = new StudentService();
        }
        return instance;
    }

    private boolean isValidEmail(String email) {
        if (email == null || email.isEmpty()) {
            return false;
        }

        String emailRegex = "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}";
        Pattern pattern = Pattern.compile(emailRegex);
        return pattern.matcher(email).matches();
    }

    public Student getStudentByEmailAndPassword(String email, String password) {
        Student student = studentDAO.findByEmail(email);
        if (student == null) {
            return null;
        }
        if (student.getPassword() == null || student.getPassword().isEmpty()) {
            return null;
        }
        if (!BCrypt.checkpw(password, student.getPassword())) {
            return null;
        }
        return student;
    }

    public String registerStudentAccount(String name, String email, String password) {
        Student student = new Student();
        student.setFullName(name);
        if (!isValidName(name)) {
            return "Invalid name";
        }
        if (!isValidEmail(email)) {
            return "Invalid email";
        }
        if (!isValidPassword(password)) {
            return "Invalid password format";
        }
        student.setEmail(email);
        student.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
        if (studentDAO.saveNewRegisterStudent(student) == null) {
            return "Failed to register";
        }
        return "Success!";
    }

    private boolean isValidName(String name) {
        if (name == null || name.isEmpty()) {
            return false;
        }

        String emailRegex = "([a-zA-Z0-9_\\s]+)";
        Pattern pattern = Pattern.compile(emailRegex);
        return pattern.matcher(name).matches();
    }

    public boolean isEmailInUse(String email) {
        return studentDAO.findByEmail(email) != null;
    }

    public boolean isEmailisUse(String email) {
        return studentDAO.findEmail(email) != null;
    }

    public boolean isphoneisUse(String phoneNumber) {
        return studentDAO.findPhone(phoneNumber) != null;
    }

    private boolean isValidPassword(String password) {
        if (password == null || password.isEmpty()) {
            return false;
        }
        String emailRegex = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}";
        Pattern pattern = Pattern.compile(emailRegex);
        return pattern.matcher(password).matches();
    }

    public String sendErrorAddTitle(String titleString) {
        if (titleString == null || titleString.isBlank()) {
            return "This field need to be fill";
        } else {
            return null;
        }
    }

    public String sendErrorAddImg(String imgString) {
        if (imgString == null || imgString.isBlank()) {
            return "This field need to be fill";
        } else {
            return null;
        }
    }

    public void insertNewStudent(String email, String password, String fullname, String datefbirth, String enrollmentdate) {
        Student student = new Student();
        student.setEmail(email);
        student.setPassword(password);
        student.setFullName(fullname);
        student.setDateOfBirth(datefbirth);
        student.setEnrollmentDate(enrollmentdate);
        student.setRoleId(3);
        studentDAO.addStudent(student);
    }

    public Student getStudentByEmail(String email) {
        return studentDAO.findByEmail(email);
    }

    public List<Student> getStudentFilter(String active, String name, String date) {
        return studentDAO.searchStudents(active, name, date);
    }

    public boolean syncStudentAccount(Student student) {
        Student regisStudent = studentDAO.findByEmail(student.getEmail());
        int result = 0;
        if (regisStudent == null) {
            result = studentDAO.saveStudent(student);
        } else {
            result = studentDAO.updateStudentByEmail(student);
        }
        if (result == 0) {
            System.out.println("Failed to sync student account");
        }
        return result != 0;
    }

    public boolean updatePasswordStudent(Student student) {
        student.setPassword(BCrypt.hashpw(student.getPassword(), BCrypt.gensalt()));
        int result = studentDAO.updatePasswordByEmail(student);
        if (result == 0) {
            System.out.println("Failed to update student account");
        }
        return result != 0;
    }

}
