/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package services;

import daos.NewsDAO;
import entities.News;
import java.util.List;

/**
 *
 * @author FPT SHOP
 */
public class NewsService {

    private static NewsService instance;

    private NewsDAO newsDAO;

    private NewsService() {
        newsDAO = new NewsDAO();
    }

    public static NewsService getInstance() {
        if (instance == null) {
            instance = new NewsService();
        }
        return instance;
    }

    public List<News> getAllNews() {
        List<News> result = newsDAO.getAllNews();
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result;
    }
}
