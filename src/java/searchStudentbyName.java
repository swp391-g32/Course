
import daos.StudentDAO;
import entities.Student;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(name = "searchStudentbyName", urlPatterns = {"/admin/searchbyname"})
public class searchStudentbyName extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String key = request.getParameter("key");
        String active = request.getParameter("active");
        String date = request.getParameter("date");
        String itemsPerPageParam = request.getParameter("itemsPerPage");
        String roleId = request.getParameter("roleId");

        // Set default values if parameters are null
        if (key != null && key.isEmpty()) {
            key = null;
        }
        if (active != null && active.isEmpty()) {
            active = null;
        }
        if (date != null && date.isEmpty()) {
            date = null;
        }
        if (itemsPerPageParam == null || itemsPerPageParam.isEmpty()) {
            itemsPerPageParam = "6"; // Default value
        }

        int itemsPerPage = Integer.parseInt(itemsPerPageParam);

        // Log parameters for debugging
        System.out.println("Key: " + key);
        System.out.println("Active: " + active);
        System.out.println("Date: " + date);
        System.out.println("Items per page: " + itemsPerPage);

        StudentDAO s = new StudentDAO();
        List<Student> ls2 = s.searchStudents(active, key, date);

        // Pagination logic
        int page;
        int size = ls2.size();
        int num = (size % itemsPerPage == 0 ? (size / itemsPerPage) : ((size / itemsPerPage)) + 1);
        String xpage = request.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start = (page - 1) * itemsPerPage;
        int end = Math.min(page * itemsPerPage, size);
        List<Student> list = s.getListPage(ls2, start, end);

        request.setAttribute("data", list);
        request.setAttribute("page", page);
        request.setAttribute("num", num);
        request.setAttribute("key", key);
        request.setAttribute("active", active);
        request.setAttribute("date", date);
        request.setAttribute("itemsPerPage", itemsPerPage);
        request.getRequestDispatcher("search.jsp").forward(request, response);
    }
}
