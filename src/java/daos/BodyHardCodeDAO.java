/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daos;

import entities.BodyHardCode;
import entities.Course;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author FPT SHOP
 */
public class BodyHardCodeDAO extends GenericDAO<BodyHardCode>{
    
    private List<BodyHardCode> listBodyHardCode;

    public BodyHardCodeDAO() {
        listBodyHardCode = new ArrayList<>();
        listBodyHardCode.add(new BodyHardCode(1, "Welcome to University"));
        listBodyHardCode.add(new BodyHardCode(2, "Batter Education For A Better"));
        listBodyHardCode.add(new BodyHardCode(3, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the"));
        listBodyHardCode.add(new BodyHardCode(4, "Welcome to University"));
        listBodyHardCode.add(new BodyHardCode(5, "Batter Education For A Better"));
        listBodyHardCode.add(new BodyHardCode(6, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the"));
        listBodyHardCode.add(new BodyHardCode(7, "CONTACT US"));
        listBodyHardCode.add(new BodyHardCode(8, "READ MORE"));
        listBodyHardCode.add(new BodyHardCode(9, "assets/images/our-services/pic1.jpg"));
        listBodyHardCode.add(new BodyHardCode(10, "Best Industry Leaders"));
        listBodyHardCode.add(new BodyHardCode(11, "View More"));
        listBodyHardCode.add(new BodyHardCode(12, "assets/images/our-services/pic2.jpg"));
        listBodyHardCode.add(new BodyHardCode(13, "Learn Courses Online"));
        listBodyHardCode.add(new BodyHardCode(14, "View More"));
        listBodyHardCode.add(new BodyHardCode(15, "assets/images/our-services/pic3.jpg"));
        listBodyHardCode.add(new BodyHardCode(16, "Book Library & Store"));
        listBodyHardCode.add(new BodyHardCode(17, "View More"));
        listBodyHardCode.add(new BodyHardCode(18, "Popular"));
        listBodyHardCode.add(new BodyHardCode(19, "Courses"));
        listBodyHardCode.add(new BodyHardCode(20, "It is a long established fact that a reader will be distracted by the readable content of a page"));
        listBodyHardCode.add(new BodyHardCode(21, "Online Courses To Learn"));
        listBodyHardCode.add(new BodyHardCode(22, "Own Your Feature Learning New Skills Online"));
        listBodyHardCode.add(new BodyHardCode(23, "Search"));
        listBodyHardCode.add(new BodyHardCode(24, "Search"));
        listBodyHardCode.add(new BodyHardCode(25, "0"));
        listBodyHardCode.add(new BodyHardCode(26, "M"));
        listBodyHardCode.add(new BodyHardCode(27, "Over 5 million student"));
        listBodyHardCode.add(new BodyHardCode(28, "0"));
        listBodyHardCode.add(new BodyHardCode(29, "K"));
        listBodyHardCode.add(new BodyHardCode(30, "30,000 Courses."));
        listBodyHardCode.add(new BodyHardCode(31, "0"));
        listBodyHardCode.add(new BodyHardCode(32, "K"));
        listBodyHardCode.add(new BodyHardCode(33, "Learn Anythink Online."));
        listBodyHardCode.add(new BodyHardCode(34, "Recent"));
        listBodyHardCode.add(new BodyHardCode(35, "News"));
        listBodyHardCode.add(new BodyHardCode(36, "It is a established fact that a reader will be distracted by the readable content of a page"));
    }
    public List<BodyHardCode> getAllBodyHardCode() {
        return listBodyHardCode.stream().toList();
    }
}
