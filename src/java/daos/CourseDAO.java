/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daos;

import entities.Course;
import java.util.ArrayList;
import java.util.List;
import mappers.impl.CourseMapper;

/**
 *
 * @author FPT SHOP
 */
public class CourseDAO extends GenericDAO<Course> {

    private final String SELECT_ALL_COURSE_STATEMENT = "SELECT * FROM course where active = 1 ";
    private final String SELECT_COURSE_BY_ID_STATEMENT = "SELECT * FROM course where course_id = ?";
    private final String SELECT_COURSE_BY_INSTRUCTOR_ID_STATEMENT = "SELECT * FROM course where [instructor_id] = ?";
    private final String SELECT_NEXT_COURSE_STATEMENT = "SELECT * FROM course ORDER BY course_id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
    private final String SELECT_TWO_RECENT_COURSE_STATEMENT = "SELECT TOP 2 * FROM course ORDER BY ABS(DATEDIFF(day, start_date, GETDATE()))";
    private final String INSERT_COURSE_STATEMENT = "Insert into course values (?,?,?,?,?,?,?,?,?,?,?)";
    private final String DEACTIVE_COURSE_STATEMENT = "UPDATE course SET active = 0 where course_id = ?";
    private final String UPDATE_COURSE_STATEMENT = "UPDATE course "
            + "                                     SET course_name = ?, img_URL = ?"
            + "                                     , start_date = ?, end_date = ?"
            + "                                     , description = ?, category_id = ?"
            + "                                     , instructor_id = ?, price = ?"
            + "                                     , discount = ?, link_meet = ? "
            + "                                     WHERE course_id = ?";
    private final String SELECT_COURSE_MAX_ID = "SELECT * FROM course WHERE course_id = (SELECT MAX(course_id) FROM course);";
    private final String SELECT_COURSE_MIN_ID = "SELECT * FROM course WHERE course_id = (SELECT MIN(course_id) FROM course);";

    public List<Course> getAllCourse() {
        List<Course> result = executeQuery(SELECT_ALL_COURSE_STATEMENT, new CourseMapper());
        return result.isEmpty() ? null : result;
    }

    public List<Course> getNextCourse(int page, int numberOfCourseInPage) {
        List<Course> result = executeQuery(SELECT_NEXT_COURSE_STATEMENT, new CourseMapper(), 0 + numberOfCourseInPage * (page - 1), numberOfCourseInPage);
        return result.isEmpty() ? null : result;
    }

    public List<Course> getNextCourseAfterFilterAll(int categoryID, String keyName, double startPrice, double endPrice, int sort, int page, int numberOfCourseInPage) {
        String queryStatement = SELECT_ALL_COURSE_STATEMENT;
        if (categoryID != 0 || keyName != null) {
            queryStatement += "and ";
            if (categoryID != 0) {
                queryStatement += "category_id = ? and ";
            }
            if (keyName != null) {
                queryStatement += "course_name like ? and ";
            }
            if (startPrice != -1 && endPrice != -1) {
                queryStatement += "(price * (1-discount) between ? and ?) and ";
            }
            if (queryStatement.endsWith("and ")) {
                queryStatement = queryStatement.substring(0, queryStatement.length() - 4);
            }
        }
        switch (sort) {
            default:
                queryStatement += " order by course_id asc";
                break;
        }
        queryStatement += " offset ? rows fetch next ? rows only";
        System.out.println(queryStatement);
        List<Object> params = new ArrayList<>();
        if (categoryID != 0) {
            params.add(categoryID);
        }
        if (keyName != null) {
            params.add("%" + keyName + "%");
        }
        if (startPrice != -1 && endPrice != -1) {
            params.add(startPrice);
            params.add(endPrice);
        }
        params.add(0 + numberOfCourseInPage * (page - 1));
        params.add(numberOfCourseInPage);
        List<Course> result = executeQuery(queryStatement, new CourseMapper(), params.toArray());
        return result.isEmpty() ? null : result;
    }

    public int getNumberOfCourseAfterFilterAll(int categoryID, String keyName, double startPrice, double endPrice) {
        String queryStatement = SELECT_ALL_COURSE_STATEMENT;
        if (categoryID != 0 || keyName != null) {
            queryStatement += "and ";
            if (categoryID != 0) {
                queryStatement += "category_id = ? and ";
            }
            if (keyName != null) {
                queryStatement += "course_name like ? and ";
            }
            if (startPrice != -1 && endPrice != -1) {
                queryStatement += "(price * (1-discount) between ? and ?) and ";
            }
            if (queryStatement.endsWith("and ")) {
                queryStatement = queryStatement.substring(0, queryStatement.length() - 4);
            }
        }
        List<Object> params = new ArrayList<>();
        if (categoryID != 0) {
            params.add(categoryID);
        }
        if (keyName != null) {
            params.add("%" + keyName + "%");
        }
        if (startPrice != -1 && endPrice != -1) {
            params.add(startPrice);
            params.add(endPrice);
        }
        List<Course> result = executeQuery(queryStatement, new CourseMapper(), params.toArray());
        return result != null ? result.size() : 0;
    }

    public List<Course> get2RecentCourse() {
        List<Course> result = executeQuery(SELECT_TWO_RECENT_COURSE_STATEMENT, new CourseMapper());
        return result.isEmpty() ? null : result;
    }

    public Course getCourseById(int id) {
        List<Course> result = executeQuery(SELECT_COURSE_BY_ID_STATEMENT, new CourseMapper(), id);
        return result.isEmpty() ? null : result.get(0);
    }
    public List<Course> getCourseByInstructorId(int id) {
        List<Course> result = executeQuery(SELECT_COURSE_BY_INSTRUCTOR_ID_STATEMENT, new CourseMapper(), id);
        return result;
    }

    public Integer insertCourse(Course course) {
        return executeUpdate(INSERT_COURSE_STATEMENT, course.getName(), course.getImgURL(),
                course.getStartDate(), course.getEndDate(), course.getDescription(),
                course.getCategoryID(), course.getInstructorID(), course.getActive(),
                course.getPrice(), course.getDiscount(), course.getLinkMeet());
    }

    public Integer deactiveCourseById(int id) {
        return executeUpdate(DEACTIVE_COURSE_STATEMENT, id);
    }

    public Integer updateCourse(Course course) {
        return executeUpdate(UPDATE_COURSE_STATEMENT, course.getName(), course.getImgURL(),
                course.getStartDate(), course.getEndDate(), course.getDescription(),
                course.getCategoryID(), course.getInstructorID(), course.getPrice(),
                course.getDiscount(), course.getLinkMeet(), course.getId());
    }

    public Course getCourseMaxID() {
        List<Course> result = executeQuery(SELECT_COURSE_MAX_ID, new CourseMapper());
        return result.isEmpty() ? null : result.get(0);
    }

    public Course getCourseMinID() {
        List<Course> result = executeQuery(SELECT_COURSE_MIN_ID, new CourseMapper());
        return result.isEmpty() ? null : result.get(0);
    }

    public static void main(String[] args) {
        CourseDAO dao = new CourseDAO();
        System.out.println(dao.getAllCourse().get(0).getImgURL());
    }
}
