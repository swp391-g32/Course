package daos;

import com.google.api.client.util.DateTime;
import entities.Student;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import mappers.impl.StudentMapper;

import java.util.List;

public class StudentDAO extends GenericDAO<Student> {

    private final String FIND_STUDENT_BY_EMAIL = "SELECT * FROM student WHERE email = ? AND active = 1";
    private final String FIND_EMAIL = "SELECT * FROM student WHERE email = ?";
    private final String FIND_PHONE = "select * from student where phone_number = ?";
    private final String SAVE_NEW_REGISTER_STUDENT = "INSERT INTO [dbo].[student] (email, password, full_name, role_id) VALUES (?, ?, ?, 3)";
    private final String SAVE_STUDENT_STATEMENT = "INSERT INTO student (email, email_verified, full_name, picture_Url, family_name, given_name, role_id) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?)";

    public Integer saveNewRegisterStudent(Student student) {
        return executeUpdate(SAVE_NEW_REGISTER_STUDENT, student.getEmail(), student.getPassword(), student.getFullName());
    }

    public Integer saveStudent(Student student) {

        return executeUpdate(SAVE_STUDENT_STATEMENT, student.getEmail(), student.getEmailVerified(),
                student.getFullName(), student.getPictureUrl(), student.getFamilyName(),
                student.getGivenName(), 3);
    }

    public Integer updatePasswordByEmail(Student student) {
        String sql = "UPDATE student SET password = ? WHERE email = ?";
        return executeUpdate(sql, student.getPassword(), student.getEmail());
    }

    public Integer updateStudentByEmail(Student student) {
        String sql = "UPDATE student SET email_verified = ?, picture_Url = ?, family_name = ?, given_name = ? WHERE email = ?";

        return executeUpdate(sql, student.getEmailVerified(), student.getPictureUrl(),
                student.getFamilyName(), student.getGivenName(), student.getEmail());
    }

    public Student findByEmail(String email) {
        List<Student> result = executeQuery(FIND_STUDENT_BY_EMAIL, new StudentMapper(), email);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    public Student findEmail(String email) {
        List<Student> result = executeQuery(FIND_EMAIL, new StudentMapper(), email);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    public Student findPhone(String phoneNumber) {
        List<Student> result = executeQuery(FIND_PHONE, new StudentMapper(), phoneNumber);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    public List<Student> getAll() {
        String sql = "select * from student";
        List<Student> list = executeQuery(sql, new StudentMapper());
        return list;
    }

    public List<Student> getByroleID(int role_id) {
        String sql = "select * from student where role_id = ? ";
        List<Student> list = executeQuery(sql, new StudentMapper(), role_id);
        return list;
    }

    public void deleteStudent(int sid) {
        String sql = "DELETE FROM student WHERE student_id = ?";
        Integer id = executeUpdate(sql, sid);
        if (id != null) {
            System.out.println("Student with ID " + sid + " has been deleted successfully.");
        } else {
            System.out.println("Failed to delete student with ID " + sid + ".");
        }

    }

    public boolean isEmailInUse(String email) {
        Student student = findByEmail(email);
        return student != null;
    }

    public boolean isEmailisUse(String email) {
        Student student = findEmail(email);
        return student != null;
    }

    public boolean isphoneisUse(String phoneNumber) {
        Student student = findPhone(phoneNumber);
        return student != null;
    }

    public Student addStudent(Student student) {
        String sql = "INSERT INTO [dbo].[student]\n"
                + "           ([email]\n"
                + "           ,[email_verified]\n"
                + "           ,[phone_number]\n"
                + "           ,[active]\n"
                + "           ,[password]\n"
                + "           ,[full_name]\n"
                + "           ,[date_of_birth]\n"
                + "           ,[picture_Url]\n"
                + "           ,[family_name]\n"
                + "           ,[given_name]\n"
                + "           ,[enrollment_date]\n"
                + "           ,[role_id])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?,?,?,?,?,?)";
        Integer generatedId = executeUpdate(sql,
                student.getEmail(),
                student.getEmailVerified(),
                student.getPhoneNumber(),
                student.getActive(),
                student.getPassword(),
                student.getFullName(),
                student.getDateOfBirth(),
                student.getPictureUrl(),
                student.getFamilyName(),
                student.getGivenName(),
                student.getEnrollmentDate(),
                student.getRoleId()
        );
        if (generatedId != null) {
            student.setId(generatedId);
        }

        return student;
    }

    public Student updateStudent(Student student) {
        String sql = "UPDATE [dbo].[student]\n"
                + "   SET [email] = ?\n"
                + "      ,[email_verified] = ?\n"
                + "      ,[phone_number] = ?\n"
                + "      ,[active] = ?\n"
                + "      ,[password] = ?\n"
                + "      ,[full_name] = ?\n"
                + "      ,[date_of_birth] = ?\n"
                + "      ,[picture_Url] = ?\n"
                + "      ,[family_name] = ?\n"
                + "      ,[given_name] = ?\n"
                + "      ,[enrollment_date] = ?\n"
                + "      ,[role_id] = ?\n"
                + " WHERE student_id = ?";
        Integer generatedId = executeUpdate(sql,
                student.getEmail(),
                student.getEmailVerified(),
                student.getPhoneNumber(),
                student.getActive(),
                student.getPassword(),
                student.getFullName(),
                student.getDateOfBirth(),
                student.getPictureUrl(),
                student.getFamilyName(),
                student.getGivenName(),
                student.getEnrollmentDate(),
                student.getRoleId(),
                student.getId()
        );
        if (generatedId != null) {
            student.setId(generatedId);
        }

        return student;
    }

    public Student getStudentById(int student_id) {
        String sql = "select * from student where student_id =?";
        List<Student> result = executeQuery(sql, new StudentMapper(), student_id);
        return result.isEmpty() ? null : result.get(0);
    }

    public List<Student> getListPage(List<Student> list, int start, int end) {
        ArrayList<Student> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;

    }

    public List<Student> searchByName(String key) {
        String queryStatement = "SELECT * FROM student";

        List<Object> params = new ArrayList<>();

        if (key != null && !key.trim().isEmpty()) {
            queryStatement += " WHERE (";
            String[] keywords = key.trim().split("\\s+");
            for (int i = 0; i < keywords.length; i++) {
                if (i > 0) {
                    queryStatement += " OR ";
                }
                queryStatement += "LOWER(full_name) LIKE ?";
                params.add("%" + keywords[i].toLowerCase() + "%");
            }
            queryStatement += ")";
        }

        return executeQuery(queryStatement, new StudentMapper(), params.toArray());
    }

    public List<Student> checkStudent(int[] id) {
        String sql = "select * from student where 1=1";
        if (id != null) {
            sql += "and role_id in (";
            for (int i = 0; i < id.length; i++) {
                sql += id[i] + ",";
            }
            if (sql.endsWith(",")) {
                sql = sql.substring(0, sql.length() - 1);
            }
            sql += ")";
        }
        return executeQuery(sql, new StudentMapper(), id);
    }

    public List<Student> searchStudents(String active, String name, String date) {
        StringBuilder sql = new StringBuilder("SELECT * FROM student WHERE 1=1");
        List<Object> params = new ArrayList<>();

        if (active != null) {
            sql.append(" AND active = ?");
            params.add(active);
        }

        if (name != null && !name.trim().isEmpty()) {
            name = name.trim().replaceAll("\\s+", " ");
            String[] keywords = name.split("\\s+");
            sql.append(" AND (");

            // Building dynamic SQL with combinations
            for (int i = 0; i < keywords.length; i++) {
                if (i > 0) {
                    sql.append(" OR ");
                }
                sql.append("LOWER(REPLACE(full_name, ' ', '')) LIKE ?");
                params.add("%" + keywords[i].toLowerCase() + "%");
            }

            // Check for combinations
            for (int i = 0; i < keywords.length; i++) {
                for (int j = 0; j < keywords.length; j++) {
                    if (i != j) {
                        sql.append(" OR LOWER(REPLACE(full_name, ' ', '')) LIKE ?");
                        params.add("%" + keywords[i].toLowerCase() + "%" + keywords[j].toLowerCase() + "%");
                    }
                }
            }

            sql.append(")");
        }

        if (date != null) {
            sql.append(" AND date = ?");
            params.add(date);
        }

        return executeQuery(sql.toString(), new StudentMapper(), params.toArray());
    }

    public static void main(String[] args) {
        StudentDAO dao = new StudentDAO();

        dao.addStudent(new Student(0, "linhvt1@gmail.com", Boolean.TRUE, "0987216161", Boolean.TRUE, "123123123123", "cheng cheng", "2003-12-12", "hienchuaco", "day roi", "day roi", "2021-12-12", 1));
//        List<Student> list1 = dao.searchStudents("0", "a", null);
//        for (Student student : list1) {
//            System.out.println(student);
//        }
    }

    public Student getStudentByEmail(String email) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
