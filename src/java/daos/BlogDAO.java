/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daos;


import entities.Blog;
import java.util.List;
import mappers.impl.BlogMapper;

/**
 *
 * @author khanh
 */
public class BlogDAO extends GenericDAO<Blog>  {
     private final String SELECT_ALL_BLOG_STATEMENT = "SELECT * FROM blog";
    private final String SELECT_BLOG_BY_ID_STATEMENT = "SELECT * FROM category_blog where blog_id = ?";
        public List<Blog> getAllBlog() {
        List<Blog> result = executeQuery(SELECT_ALL_BLOG_STATEMENT, new BlogMapper());
        return result.isEmpty() ? null : result;
    }

    public Blog getBlogById(int id) {
        List<Blog> result = executeQuery(SELECT_BLOG_BY_ID_STATEMENT, new BlogMapper(), id);
        return result.isEmpty() ? null : result.get(0);
    }
}
