/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daos;

import entities.Footer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author quang huy
 */
public class FooterDAO extends GenericDAO<Footer> {
    
    private List<Footer> listFooter;
    
    public FooterDAO() {
        listFooter = new ArrayList<>();
        listFooter.add(new Footer("logo", "/FPT-Nihongo/assets/images/logo-white.png"));
        listFooter.add(new Footer("footerWidget1_1", "HomePage"));
        listFooter.add(new Footer("footerWidget1_2", "About Us"));
        listFooter.add(new Footer("footerWidget1_3", "FAQs"));
        listFooter.add(new Footer("footerWidget1_4", "Contact"));
        listFooter.add(new Footer("footerWidget2_1", "Dashboard"));
        listFooter.add(new Footer("footerWidget2_2", "Blog"));
        listFooter.add(new Footer("footerWidget2_3", "Event"));
        listFooter.add(new Footer("footerWidget3_1", "Course"));
        listFooter.add(new Footer("footerWidget3_2", "Details"));
        listFooter.add(new Footer("footerWidget3_3", "Profile"));
      
    }
    
    public List<Footer> getListFooter() {
        List<Footer> result = new ArrayList<>();
        for (Footer footer : listFooter) {
                result.add(footer);
        }
        if (result.isEmpty()) {
            return null;
        }
        return result;
    }
}
