/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daos;

import entities.Lecture;
import java.util.ArrayList;
import java.util.List;
import mappers.impl.LectureMapper;

public class LectureDAO extends GenericDAO<Lecture> {

    private final String SELECT_LECTURE_BY_ID_STATEMENT = "SELECT * FROM lecture where lecture_id = ?";
    private final String SELECT_ALL_LECTURE_STATEMENT = "SELECT * FROM lecture";
    private final String SELECT_ALL_LECTURE_BY_COURSEID = "SELECT * FROM Lecture where course_id=?";
    private final String COUNT_ALL_LECTURE_BY_COURSEID = "SELECT COUNT(*) FROM lecture WHERE course_id = ?";
    private final String SELECT_NEXT_LECTURE_STATEMENT = " SELECT * FROM lecture ORDER BY lecture_id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
    private final String TOTAL_COURSE_HOURS = "SELECT SUM(time)\n"
            + "FROM lecture\n"
            + "WHERE course_id = ?\n"
            + "GROUP BY course_id";
    private final String INSERT_LECTURE_STATEMENT = "Insert into lecture values (?,?,?,?,?,?)";
    private final String DELETE_LECTURE_STATEMENT = "DELETE FROM lecture WHERE lecture_id=?";
    private final String SELECT_LECTURE_MAX_ID = "SELECT * FROM lecture WHERE lecture_id = (SELECT MAX(lecture_id) FROM lecture);";
    private final String SELECT_LECTURE_MIN_ID = "SELECT * FROM lecture WHERE lecture_id = (SELECT MIN(lecture_id) FROM lecture);";
    private final String UPDATE_LECTURE_STATEMENT = "UPDATE lecture "
            + "SET lecture_name = ?,time = ?"
            + "description = ?, image_url = ?, video_url = ?"
            + " Where lecture_id = ?";

    public List<Lecture> getAllLectureByCourseId(int courseID) {
        List<Lecture> result = executeQuery(SELECT_ALL_LECTURE_BY_COURSEID, new LectureMapper(), courseID);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result;

    }

    public Lecture getLectureById(int id) {
        List<Lecture> result = executeQuery(SELECT_LECTURE_BY_ID_STATEMENT, new LectureMapper(), id);
        return result.isEmpty() ? null : result.get(0);
    }

    public Lecture getLectureMaxID() {
        List<Lecture> result = executeQuery(SELECT_LECTURE_MAX_ID, new LectureMapper());
        return result.isEmpty() ? null : result.get(0);
    }

    public Lecture getLectureMinID() {
        List<Lecture> result = executeQuery(SELECT_LECTURE_MIN_ID, new LectureMapper());
        return result.isEmpty() ? null : result.get(0);
    }

    public int countAllLectureByCourseId(int courseID) {
        int count = count(COUNT_ALL_LECTURE_BY_COURSEID, courseID);

        return count;
    }

    public int getTotalCourseHours(int courseID) {
        int totalTimes = count(TOTAL_COURSE_HOURS, courseID);
        return totalTimes;
    }

    public Integer insertLecture(Lecture lecture) {
        return executeUpdate(INSERT_LECTURE_STATEMENT, lecture.getName(), lecture.getDescription(), lecture.getTime(),
                lecture.getImageURL(), lecture.getVideoURL(), lecture.getCourseID());
    }

    public List<Lecture> getNextLecture(int page, int numberOfLectureInPage) {
        List<Lecture> result = executeQuery(SELECT_NEXT_LECTURE_STATEMENT, new LectureMapper(), 0 + numberOfLectureInPage * (page - 1), numberOfLectureInPage);
        return result.isEmpty() ? null : result;
    }

    public static void main(String[] args) {
        LectureDAO dao = new LectureDAO();
        List<Lecture> list = dao.getNextLectureAfterFilterAll("", 1, 1, 9);
        System.out.println(list);

    }

    public List<Lecture> getNextLectureAfterFilterAll(String keyName, int sort, int page, int numberOfLectureInPage) {
        String queryStatement = SELECT_ALL_LECTURE_STATEMENT;
        if (keyName != null) {
            queryStatement += " where ";

            if (keyName != null) {
                queryStatement += "lecture_name like ? ";
            }

        }
        switch (sort) {
            default:
                queryStatement += " order by lecture_id asc";
                break;
        }
        queryStatement += " offset ? rows fetch next ? rows only";
        System.out.println(queryStatement);
        List<Object> params = new ArrayList<>();

        if (keyName != null) {
            params.add("%" + keyName + "%");
        }

        params.add(0 + numberOfLectureInPage * (page - 1));
        params.add(numberOfLectureInPage);
        List<Lecture> result = executeQuery(queryStatement, new LectureMapper(), params.toArray());
        return result.isEmpty() ? null : result;
    }

    public Integer deleteLectureById(int id) {
        return executeUpdate(DELETE_LECTURE_STATEMENT, id);
    }

    public double getNumberOfLectureAfterFilterAll(String keyName) {
        String queryStatement = SELECT_ALL_LECTURE_STATEMENT;
        if (keyName != null) {
            queryStatement += " where ";

            if (keyName != null) {
                queryStatement += "course_name like ? ";
            }

        }
        List<Object> params = new ArrayList<>();

        if (keyName != null) {
            params.add("%" + keyName + "%");
        }

        List<Lecture> result = executeQuery(queryStatement, new LectureMapper(), params.toArray());
        return result != null ? result.size() : 0;
    }

    public Integer updateLecture(Lecture lecture) {
        return executeUpdate(UPDATE_LECTURE_STATEMENT, lecture.getName(),lecture.getTime(),lecture.getDescription(), 
                lecture.getImageURL(),lecture.getVideoURL(),lecture.getId());
    }

}
