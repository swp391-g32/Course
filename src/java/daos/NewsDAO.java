/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daos;

import entities.News;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author FPT SHOP
 */
public class NewsDAO extends GenericDAO<News> {

    private List<News> listNews;

    public NewsDAO() {
        listNews = new ArrayList<>();
        listNews.add(new News(1, "Introduce new courses for beginner",
                "Jan 02 2019", "John",
                "Beginner coding usually involves learning a basic programming language such as HTML or CSS before advancing into more rigorous languages such as C or Python",
                23,
                "assets/images/blog/latest-blog/pic1.jpg"));
        listNews.add(new News(2,
                "Best sale courses for you",
                "April 14 2019",
                "William",
                "Elevate your skills with our best-selling course! Dive into practical knowledge and hands-on experience in Programing. Limited time offer, enroll now and unleash your potential",
                21,
                "assets/images/blog/latest-blog/pic2.jpg"));
        listNews.add(new News(3,
                "Introducing the new Python programming course",
                "Feb 05 2019",
                "Penny",
                "Python is a computer programming language often used to build websites and software, automate tasks, and conduct data analysis. Python is a general-purpose language, meaning it can be used to create a variety of different programs and isn't specialized for any specific problems.",
                40,
                "assets/images/blog/latest-blog/pic3.jpg"));
    }

    public List<News> getAllNews() {
        return listNews.stream().toList();
    }

    public static void main(String[] args) {
        NewsDAO dao = new NewsDAO();
        List<News> list = dao.getAllNews();
        for (News i : list) {
            System.out.println(i.getTilte());
        }
    }
}
