package daos;

import entities.Admin;
import mappers.impl.AdminMapper;

import java.util.List;

public class AdminDAO extends GenericDAO<Admin> {

    private final String FIND_ADMIN_BY_EMAIL = "SELECT * FROM admin WHERE email = ? AND active = 1";

    public AdminDAO() {

    }

    public Admin findByEmail(String email) {
        List<Admin> result = executeQuery(FIND_ADMIN_BY_EMAIL, new AdminMapper(), email);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }


}