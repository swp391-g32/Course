package daos;

import entities.Instructor;
import mappers.impl.InstructorMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InstructorDAO extends GenericDAO<Instructor> {

    private final String FIND_BY_EMAIL = "SELECT * FROM instructor WHERE email = ?";
    private final String SELECT_ALL_INSTRUCTOR_STATEMENT = "SELECT * FROM instructor";
    private final String SELECT_INSTRUCTOR_BY_ID_STATEMENT = "SELECT * FROM instructor where instructor_id = ?";
    private final String SELECT_INSTRUCTOR_BY_COURSEID = "SELECT *\n"
            + "FROM [dbo].[instructor] i\n"
            + "INNER JOIN [dbo].[course] c ON i.[instructor_id] = c.[instructor_id]\n"
            + "WHERE c.[course_id] = ?";

    public Instructor findByEmail(String email) {
        List<Instructor> result = executeQuery(FIND_BY_EMAIL, new InstructorMapper(), email);
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    public List<Instructor> getAllInstructor() {
        List<Instructor> result = executeQuery(SELECT_ALL_INSTRUCTOR_STATEMENT, new InstructorMapper());
        return result.isEmpty() ? null : result;
    }

    public Instructor getInstructorById(int id) {
        List<Instructor> result = executeQuery(SELECT_INSTRUCTOR_BY_ID_STATEMENT, new InstructorMapper(), id);
        return result.isEmpty() ? null : result.get(0);
    }

    public Instructor getInstructorByCourseId(int courseId) {
        List<Instructor> result = executeQuery(SELECT_INSTRUCTOR_BY_COURSEID, new InstructorMapper(), courseId);
        return result.isEmpty() ? null : result.get(0);
    }

   public static void main(String[] args) {
       InstructorDAO ins= new InstructorDAO();
       Instructor i= ins.getInstructorByCourseId(1);
       System.out.println(i.getEmail()); 
   }
}
