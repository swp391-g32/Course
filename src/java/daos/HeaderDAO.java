/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daos;

import entities.Header;
import java.util.ArrayList;
import java.util.List;

public class HeaderDAO extends GenericDAO<Header> {

    private List<Header> listHeader;

    public HeaderDAO() {
        listHeader = new ArrayList<>();
        listHeader.add(new Header("1", "https://w7.pngwing.com/pngs/551/211/png-transparent-education-logo-pre-school-others-text-logo-business-thumbnail.png"));
        listHeader.add(new Header("main_menu_1", "HomePage"));
        listHeader.add(new Header("3", "About Us"));
        listHeader.add(new Header("4", "Our Course"));
        listHeader.add(new Header("5", "Blog"));
         listHeader.add(new Header("logo", "/FPT-Nihongo/assets/images/logo-white.png"));
        listHeader.add(new Header("footerWidget1_1", "HomePage"));
        listHeader.add(new Header("footerWidget1_2", "About Us"));
        listHeader.add(new Header("footerWidget1_3", "FAQs"));
        listHeader.add(new Header("footerWidget1_4", "Contact"));
        listHeader.add(new Header("footerWidget2_1", "Dashboard"));
        listHeader.add(new Header("footerWidget2_2", "Blog"));
        listHeader.add(new Header("footerWidget2_3", "Event"));
        listHeader.add(new Header("footerWidget3_1", "Course"));
        listHeader.add(new Header("footerWidget3_2", "Details"));
        listHeader.add(new Header("footerWidget3_3", "Profile"));
    }

//    public List<Header> getListHeader(String id, String description) {
//        List<Header> result = new ArrayList<>();
//        for (Header header : listHeader) {
//            if (header.getId().equals(id) && header.getDescription().equals(description)) {
//                result.add(header);
//            }
//        }
//        if (result.isEmpty()) {
//            return null;
//        }
//        return result;
//    }
    public List<Header> getListHeader() {
        List<Header> result = new ArrayList<>();
        for (Header header : listHeader) {
            result.add(header);
        }
        if (result.isEmpty()) {
            return null;
        }
        return result;
    }

}
