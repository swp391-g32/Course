package entities;

public class Student implements IActor {

    private int id;
    private String email;

    private Boolean emailVerified;
    private String phoneNumber;
    private Boolean active;
    private String password;
    private String fullName;
    private String dateOfBirth;
    private String pictureUrl;
    private String familyName;
    private String givenName;
    private String enrollmentDate;

    private int roleId;

    public Student() {

    }

    public Student(int id, String email, Boolean emailVerified, String phoneNumber, Boolean active, String password, String fullName, String dateOfBirth, String pictureUrl, String familyName, String givenName, String enrollmentDate, int roleId) {
        this.id = id;
        this.email = email;
        this.emailVerified = emailVerified;
        this.phoneNumber = phoneNumber;
        this.active = active;
        this.password = password;
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.pictureUrl = pictureUrl;
        this.familyName = familyName;
        this.givenName = givenName;
        this.enrollmentDate = enrollmentDate;
        this.roleId = roleId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEnrollmentDate() {
        return enrollmentDate;
    }

    public void setEnrollmentDate(String enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "Student{" + "id=" + id + ", email=" + email + ", emailVerified=" + emailVerified + ", phoneNumber=" + phoneNumber + ", active=" + active + ", password=" + password + ", fullName=" + fullName + ", dateOfBirth=" + dateOfBirth + ", pictureUrl=" + pictureUrl + ", familyName=" + familyName + ", givenName=" + givenName + ", enrollmentDate=" + enrollmentDate + ", roleId=" + roleId + '}'+"\n";
    }
    
}
