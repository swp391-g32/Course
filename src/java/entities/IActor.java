package entities;

public interface IActor {
    Integer getId();
    Integer getRoleId();
    String getEmail();
    String getPassword();
}
