/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entities;

import services.CategoryCourseService;

/**
 *
 * @author FPT SHOP
 */
public class Course {

    private int id;
    private String name;
    private String imgURL;
    private String startDate;
    private String endDate;
    private String description;
    private int categoryID;
    private int instructorID;
    private int active;
    private double price;
    private double discount;
    private String linkMeet;

    public Course() {
    }

    public Course(int id, String name, String imgURL, String startDate, String endDate, String description, int categoryID, int instructorID, int active, double price, double discount, String linkMeet) {
        this.id = id;
        this.name = name;
        this.imgURL = imgURL;
        this.startDate = startDate;
        this.endDate = endDate;
        this.description = description;
        this.categoryID = categoryID;
        this.instructorID = instructorID;
        this.active = active;
        this.price = price;
        this.discount = discount;
        this.linkMeet = linkMeet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getInstructorID() {
        return instructorID;
    }

    public void setInstructorID(int instructorID) {
        this.instructorID = instructorID;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    } 

    public String getLinkMeet() {
        return linkMeet;
    }

    public void setLinkMeet(String linkMeet) {
        this.linkMeet = linkMeet;
    }
    
    public String getCategoryCourseName(){
        CategoryCourseService categoryCourseService = new CategoryCourseService();
        return categoryCourseService.getCategoryCourseById(this.categoryID).getName();
    }
}
