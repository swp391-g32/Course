/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mappers.impl;

import entities.Course;
import java.sql.ResultSet;
import java.sql.SQLException;
import mappers.RowMapperInterface;

/**
 *
 * @author FPT SHOP
 */
public class CourseMapper implements RowMapperInterface<Course>{
    @Override
    public Course mapRow(ResultSet rs) {
        try {
            Integer id = rs.getInt("course_id");
            String name = rs.getString("course_name");
            String imgURL = rs.getString("img_URL");
            String startDate = rs.getString("start_date");
            String endDate = rs.getString("end_date");
            String description = rs.getString("description");
            Integer categoryID = rs.getInt("category_id");
            Integer instructorID = rs.getInt("instructor_id");
            Integer active = rs.getInt("active");
            Double price = rs.getDouble("price");
            Double discount = rs.getDouble("discount");
            String linkMeet = rs.getString("link_meet");
            
            return new Course(id, name, imgURL, startDate, endDate, description, categoryID, instructorID, active, price, discount, linkMeet);
        } catch (SQLException e) {
            System.out.println("Error in CourseMapper: " + e.getMessage());
            return null;
        }
    }
}
