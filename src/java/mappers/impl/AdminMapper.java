package mappers.impl;

import entities.Admin;
import mappers.RowMapperInterface;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AdminMapper implements RowMapperInterface<Admin> {
    @Override
    public Admin mapRow(ResultSet rs) {
        try {
            Admin admin = new Admin();
            admin.setId(rs.getInt("admin_id"));
            admin.setEmail(rs.getString("email"));
            admin.setActive(rs.getBoolean("active"));
            admin.setPassword(rs.getString("password"));
            admin.setRoleId(rs.getInt("role_id"));
            return admin;
        } catch (SQLException e) {
            System.out.println("Error in ManagerMapper: " + e.getMessage());
            return null;
        }
    }
}