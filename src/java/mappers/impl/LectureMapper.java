/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mappers.impl;
import entities.Lecture;
import mappers.RowMapperInterface;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LectureMapper implements RowMapperInterface<Lecture>{
     @Override
    public Lecture mapRow(ResultSet rs) {
        try {
            int id = rs.getInt("lecture_id");
            String name = rs.getString("lecture_name");
            int time=rs.getInt("time");
            String description = rs.getString("description");
            String imageURL = rs.getString("image_url");
            String videoURL = rs.getString("video_url");
            int course_id = rs.getInt("course_id");
            
            
            return new Lecture(id, name,time, description, imageURL, videoURL, course_id);
        } catch (SQLException e) {
            System.out.println("Error in LectureMapper: " + e.getMessage());
            return null;
        }
    }
}
