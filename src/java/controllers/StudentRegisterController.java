package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import services.StudentService;

import java.io.IOException;

@WebServlet(name = "SignUpController", urlPatterns = {"/signup"})
public class StudentRegisterController extends HttpServlet {

    private StudentService studentService;

    @Override
    public void init() throws ServletException {
        studentService = StudentService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("register.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        String message = studentService.registerStudentAccount(name, email, password);
        req.setAttribute("registerMessage", message);
        req.setAttribute("name", name);
        req.setAttribute("email", email);
        req.setAttribute("password", password);
        if(message.equals("Success!")){
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        } else {
            req.getRequestDispatcher("register.jsp").forward(req, resp);
        }
    }
}
