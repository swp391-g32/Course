/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.AccessDeniedException;
import java.nio.file.Paths;
import services.CategoryCourseService;
import services.CourseService;
import services.InstructorService;
import services.LectureService;

/**
 *
 * @author quang huy
 */
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 100 // 100 MB
)
@WebServlet(name = "AddLectureController", urlPatterns = {"/admin/add-lecture"})
public class AddLectureController extends HttpServlet {

    private CategoryCourseService categoryCourseService;
    private InstructorService instructorService;
    private CourseService courseService;
    private LectureService lectureService;

    @Override
    public void init() throws ServletException {
        courseService = CourseService.getInstance();
        categoryCourseService = CategoryCourseService.getInstance();
        instructorService = InstructorService.getInstance();
        lectureService = LectureService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String instructorId = request.getParameter("inId");
        if (instructorId == null || instructorId.isBlank()) {
            request.getRequestDispatcher("error-404.html").forward(request, response);
            return;
        }
        request.setAttribute("courseList", courseService.getCourseByInstructorId(instructorId));

        request.getRequestDispatcher("add-lecture.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            String lecture_name = (String) request.getParameter("lecture_name");
            String lecture_description = (String) request.getParameter("lecture_description");
            String lecture_time = (String) request.getParameter("lecture_time");
            String video = (String) request.getParameter("video");
            String course_id = (String) request.getParameter("coursr_id");
            
            String errorTitle = lectureService.sendErrorAddTitle(lecture_name);
            String errorDescription=lectureService.sendErrorAddTitle(lecture_description);
            String errorTime=lectureService.sendErrorAddTime(lecture_time);
            String errorVideo=lectureService.sendErrorAddTitle(video);

            Part part = request.getPart("file");
            String fileName = Paths.get(part.getSubmittedFileName()).getFileName().toString();
            String uploadPath = getServletContext().getRealPath("") + File.separator + "img";

            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            part.write(uploadPath + File.separator + fileName);
            String imageURL = "../img/" + fileName;
       //     String errorImage=lectureService.sendErrorAddImage(part);
            
            if(errorTitle==null && errorDescription==null && errorTime==null && errorVideo==null){
            lectureService.insertNewLecture(lecture_name, lecture_description, lecture_time, imageURL, video, course_id);
            request.setAttribute("buildSuccess", "Add lecture successfully!");
            }
            if(errorTime!=null){
                request.setAttribute("errorTime", errorTime);
            }
            if(errorTitle!=null){
                request.setAttribute("errorTitle", errorTitle);
            }
            if(errorDescription!=null){
                request.setAttribute("errorDescription", errorDescription);
            }
//            if(errorImage!=null){
//                request.setAttribute("errorImage", errorImage);
//            }
            if(errorVideo!=null){
                request.setAttribute("errorVideo", errorVideo);
            }
            
            request.setAttribute("courseList", courseService.getAllCourse());
            request.getRequestDispatcher("add-lecture.jsp").forward(request, response);
        }catch(AccessDeniedException e){
            request.getRequestDispatcher("error-404.html").forward(request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    public File getFolderUpload() {
        String uploadPath = getServletContext().getRealPath("") + File.separator + "img";

        File folderUpload = new File(uploadPath);
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }
}
