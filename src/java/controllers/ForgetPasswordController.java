package controllers;

import entities.Student;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import services.EmailFactory;
import services.EmailService;
import services.StudentService;
import services.TokenService;

import java.io.IOException;

@WebServlet(name = "ForgetPasswordController", value = "/forget-password")
public class ForgetPasswordController extends HttpServlet {

    private StudentService studentService;
    private TokenService tokenService;
    private EmailService emailService;
    @Override
    public void init() throws ServletException {
        studentService = StudentService.getInstance();
        tokenService = new TokenService();
        emailService = EmailService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("forget-password.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");

        // Check if the email exists in the database
        Student student = studentService.getStudentByEmail(email);
        if (student == null) {
            req.setAttribute("message", "Email does not exist.");
            req.getRequestDispatcher("forget-password.jsp").forward(req, resp);
            return;
        }
        // Generate a token for the user
        String token = tokenService.generateToken(student);
        // Build the email content
        String content = EmailFactory.getInstance().generateResetPasswordEmail(token);

        // Send the email
        emailService.sendEmail(email, "Reset Password", content);

        req.setAttribute("message", "An email has been sent to your email address. Please check your email to reset your password.");
        req.getRequestDispatcher("forget-password.jsp").forward(req, resp);
    }
}
