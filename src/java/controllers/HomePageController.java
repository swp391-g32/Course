/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import daos.FooterDAO;
import daos.HeaderDAO;
import entities.BodyHardCode;
import entities.Course;
import entities.Footer;
import entities.Header;
import entities.News;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import services.BodyHardCodeService;
import services.CourseService;
import services.NewsService;

/**
 *
 * @author FPT SHOP
 */
@WebServlet(name = "HomePageController", urlPatterns = {"/home"})
public class HomePageController extends HttpServlet {

    private CourseService courseService;
    private NewsService newsService;
    private BodyHardCodeService bodyHardCodeService;

    @Override
    public void init() throws ServletException {
        courseService = CourseService.getInstance();
        newsService = NewsService.getInstance();
        bodyHardCodeService = bodyHardCodeService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Course> listCourse = courseService.getAllCourse();
        req.setAttribute("listcourse", listCourse);

        List<News> listNews = newsService.getAllNews();
        req.setAttribute("listnews", listNews);

        HeaderDAO headerDAO = new HeaderDAO();
        List<Header> listHeader = headerDAO.getListHeader();
        req.setAttribute("listHeader", listHeader);
        
        FooterDAO footerDAO = new FooterDAO();
        List<Footer> listFooter = footerDAO.getListFooter(); 
        req.setAttribute("listFooter", listFooter);
        
        List<BodyHardCode> listBodyHardCode = bodyHardCodeService.getAllBodyHardCode();
        req.setAttribute("listbodyhardcode", listBodyHardCode);
        
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
