package controllers;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import entities.Student;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import services.JwtTokenProvider;
import services.StudentService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@WebServlet("/oauth2callback")
public class OAuth2CallbackController extends HttpServlet {

    private final String CLIENT_ID = "452415221272-go3joh8nlbfmtrct8ge3b2vkg8jp484n.apps.googleusercontent.com";
    private final Integer COOKIE_LIFE_TIME = 60 * 60 * 24;

    private StudentService studentService;

    @Override
    public void init() throws ServletException {
        studentService = StudentService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("login");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String idTokenString = req.getParameter("credential");
        Student student = getStudentBy(idTokenString);

        if( student == null){
            resp.sendRedirect("login");
        } else {
            if(studentService.syncStudentAccount(student)){
                JwtTokenProvider jwtTokenProvider = new JwtTokenProvider();
                Student account = studentService.getStudentByEmail(student.getEmail());
                setCookie(resp, "student_token", jwtTokenProvider.generateToken(account));
                resp.sendRedirect("home");
            } else {
                resp.sendRedirect("login");
            }
        }

    }

    private void setCookie(HttpServletResponse resp, String cookieName, String token){
        Cookie cookie = new Cookie(cookieName, token);
        cookie.setMaxAge(COOKIE_LIFE_TIME);
        cookie.setDomain("localhost");
        cookie.setPath("/FPT-Nihongo");
        cookie.setHttpOnly(true);
        resp.addCookie(cookie);
    }

    private Student getStudentBy(String idTokenString) {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new GsonFactory())
                .setAudience(Collections.singletonList(CLIENT_ID))
                .build();
        try {
            GoogleIdToken idToken = verifier.verify(idTokenString);
            if (idToken != null) {
                Payload payload = idToken.getPayload();
                Student student = new Student();
                student.setEmail(payload.getEmail());
                student.setEmailVerified(payload.getEmailVerified());
                student.setFullName((String) payload.get("name"));
                student.setPictureUrl((String) payload.get("picture"));
                student.setFamilyName((String) payload.get("family_name"));
                student.setGivenName((String) payload.get("given_name"));

                return student;
            } else {
                System.out.println("Invalid ID token.");
            }
        } catch (GeneralSecurityException | IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }


}