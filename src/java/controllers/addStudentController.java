package controllers;

import daos.StudentDAO;
import entities.Student;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.regex.Pattern;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import jakarta.servlet.annotation.MultipartConfig;
import java.util.Map;

@WebServlet(name = "addStudentController", urlPatterns = {"/admin/addStudent"})
@MultipartConfig
public class addStudentController extends HttpServlet {

    @Override
    public void init() throws ServletException {
        // Tạo thư mục 'images' khi khởi động ứng dụng nếu nó không tồn tại
        String appPath = getServletContext().getRealPath("");
        String uploadPath = appPath + File.separator + "images";
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Handle initial page load
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Lấy danh sách sinh viên từ cơ sở dữ liệu
     
        StudentDAO dao = new StudentDAO();
        List<Student> studentList = dao.getAll();

        // Chuyển hướng tới trang addStudent.jsp và gửi danh sách sinh viên
        request.setAttribute("studentList", studentList);
        request.getRequestDispatcher("addStudent.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        // Lấy dữ liệu từ form
        String email = request.getParameter("email");
        String emailVerifiedStr = request.getParameter("email_verified");
        String phoneNumber = request.getParameter("phoneNumber");
        String activeStr = request.getParameter("active");
        String password = request.getParameter("password");
        String fullName = request.getParameter("fullName");
        String dateOfBirth = request.getParameter("dateOfBirth");
        String familyName = request.getParameter("familyName");
        String givenName = request.getParameter("givenName");
        String enrollmentDate = request.getParameter("enrollmentDate");
        String roleIdStr = request.getParameter("roleId");

        boolean hasErrors = false;

        // Validation
        String errorEmail = validateEmail(email);
        String errorEmailVerified = validateBoolean(emailVerifiedStr);
        String errorPhoneNumber = validatePhoneNumber(phoneNumber);
        String errorActive = validateBoolean(activeStr);
        String errorPassword = validatePassword(password);
        String errorFullName = validateFullName(fullName);
        String errorDateOfBirth = validateDateOfBirth(dateOfBirth);
        String errorEnrollmentDate = validateDate(enrollmentDate);
        String errorRoleId = validateInteger(roleIdStr);
        String errorPictureUrl = validateImage(request);

        // Kiểm tra lỗi
        if (errorEmail != null || errorEmailVerified != null || errorPhoneNumber != null || errorActive != null
                || errorPassword != null || errorFullName != null || errorDateOfBirth != null || errorEnrollmentDate != null
                || errorRoleId != null || errorPictureUrl != null) {
            hasErrors = true;
        }

        if (!hasErrors) {
            try {
                // Chuyển đổi chuỗi sang kiểu dữ liệu tương ứng
                boolean emailVerified = Boolean.parseBoolean(emailVerifiedStr);
                boolean active = Boolean.parseBoolean(activeStr);
                int roleId = roleIdStr != null ? Integer.parseInt(roleIdStr) : 1;

                // Kiểm tra sinh viên đã tồn tại hay chưa
                StudentDAO dao = new StudentDAO();
                Student existingStudent = dao.findByEmail(email);

                String fileName = "";
                if (existingStudent == null) {
                    // Sinh viên mới
                    // Lưu hình ảnh vào thư mục upload
                    fileName = handleFileUpload(request);
                    if (fileName == null) {
                        request.setAttribute("error", "Failed to upload image.");
                        request.getRequestDispatcher("addStudent.jsp").forward(request, response);
                        return;
                    }

                    // Tạo đối tượng Student và thêm vào cơ sở dữ liệu
                    Student newStudent = new Student(0, email, emailVerified, phoneNumber, active, password, fullName,
                            dateOfBirth, fileName, familyName, givenName, enrollmentDate, roleId);
                    dao.addStudent(newStudent);
                    response.sendRedirect("searchbyname");
                } else {
                    // Sinh viên đã tồn tại, cập nhật thông tin
                    fileName = handleFileUpload(request);
                    if (fileName == null || fileName.isEmpty()) {
                        fileName = existingStudent.getPictureUrl();
                    }

                    // Cập nhật thông tin sinh viên hiện có
                    existingStudent.setEmailVerified(emailVerified);
                    existingStudent.setPhoneNumber(phoneNumber);
                    existingStudent.setActive(active);
                    existingStudent.setPassword(password);
                    existingStudent.setFullName(fullName);
                    existingStudent.setDateOfBirth(dateOfBirth);
                    existingStudent.setPictureUrl(fileName);
                    existingStudent.setFamilyName(familyName);
                    existingStudent.setGivenName(givenName);
                    existingStudent.setEnrollmentDate(enrollmentDate);
                    existingStudent.setRoleId(roleId);

                    dao.updateStudent(existingStudent);
                    response.sendRedirect("searchbyname");
                }
            } catch (Exception e) {
                request.setAttribute("error", "An error occurred while processing your request: " + e.getMessage());
                e.printStackTrace();
                request.getRequestDispatcher("addStudent.jsp").forward(request, response);
            }
        } else {
            // Forward to addStudent.jsp if there are validation errors
            request.setAttribute("errorEmail", errorEmail);
            request.setAttribute("errorEmailVerified", errorEmailVerified);
            request.setAttribute("errorPhoneNumber", errorPhoneNumber);
            request.setAttribute("errorActive", errorActive);
            request.setAttribute("errorPassword", errorPassword);
            request.setAttribute("errorFullName", errorFullName);
            request.setAttribute("errorDateOfBirth", errorDateOfBirth);
            request.setAttribute("errorEnrollmentDate", errorEnrollmentDate);
            request.setAttribute("errorRoleId", errorRoleId);
            request.setAttribute("errorPictureUrl", errorPictureUrl);
            request.getRequestDispatcher("addStudent.jsp").forward(request, response);
        }
    }

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    private String validateEmail(String email) {
        if (email == null || email.isEmpty()) {
            return "Email cannot be empty.";
        } else if (services.StudentService.getInstance().isEmailisUse(email)) {
            return "Email already exists.";
        }
        String emailRegex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";
        if (!Pattern.matches(emailRegex, email)) {
            return "Invalid email format.";
        }
        return null;
    }

    private String validatePhoneNumber(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.isEmpty()) {
            return "Phone number cannot be empty.";
        } else if (services.StudentService.getInstance().isphoneisUse(phoneNumber)) {
            return "Phone already exists.";
        }
        String phoneRegex = "\\d{10,15}";
        if (!Pattern.matches(phoneRegex, phoneNumber)) {
            return "Invalid phone number format.";
        }
        return null;
    }

    private String validatePassword(String password) {
        if (password == null || password.isEmpty()) {
            return "Password cannot be empty.";
        }
        if (password.length() < 8) {
            return "Password must be at least 8 characters long.";
        }
        return null;
    }

    private String validateFullName(String fullName) {
        if (fullName == null || fullName.isEmpty()) {
            return "Full name cannot be empty.";
        }
        return null;
    }

    private String validateDateOfBirth(String dateOfBirth) {
        if (dateOfBirth == null || dateOfBirth.isEmpty()) {
            return "Date of birth cannot be empty.";
        }
        try {
            LocalDate dob = LocalDate.parse(dateOfBirth);
            LocalDate today = LocalDate.now();
            if (dob.isAfter(today.minusYears(5))) {
                return "Student must be at least 5 years old.";
            }
        } catch (DateTimeParseException e) {
            return "Invalid date of birth format.";
        }
        return null;
    }

    private String validateUrl(String url) {
        if (url == null || url.isEmpty()) {
            return "URL cannot be empty.";
        }
        return null;
    }

    private String validateDate(String date) {
        if (date == null || date.isEmpty()) {
            return "Date cannot be empty.";
        }
        try {
            LocalDate.parse(date);
        } catch (DateTimeParseException e) {
            return "Invalid date format.";
        }
        return null;
    }

    private String validateBoolean(String value) {
        if (value == null || value.isEmpty()) {
            return "This field cannot be empty.";
        }
        return null;
    }

    private String validateInteger(String value) {
        if (value == null || value.isEmpty()) {
            return "This field cannot be empty.";
        }
        try {
            Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return "Invalid integer value.";
        }
        return null;
    }

    private String validateImage(HttpServletRequest request) {
        try {
            Part filePart = request.getPart("pictureUrl");
            if (filePart != null && filePart.getSize() > 0) {
                String contentType = filePart.getContentType();
                if (!contentType.startsWith("image/")) {
                    return "File is not an image.";
                }
            }
        } catch (IOException | ServletException e) {
            return "Failed to process image upload.";
        }
        return null;
    }

    private String handleFileUpload(HttpServletRequest request) throws IOException, ServletException {
        Part filePart = request.getPart("pictureUrl");
        if (filePart != null && filePart.getSize() > 0) {
            // Validate the image
            String validationError = validateImage(request);
            if (validationError != null) {
                return null;
            }

            String appPath = request.getServletContext().getRealPath("");
            String uploadPath = appPath + File.separator + "images";
            String fileName = extractFileName(filePart);
            String filePath = uploadPath + File.separator + fileName;
            filePart.write(filePath);
            return fileName;
        }
        return null;
    }
}
