/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import entities.Lecture;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import services.CategoryCourseService;
import services.CourseService;
import services.InstructorService;
import services.LectureService;

/**
 *
 * @author quang huy
 */
@WebServlet(name="DeleteLectureController", urlPatterns={"/admin/delete-lecture"})
public class DeleteLectureController extends HttpServlet {
      private CategoryCourseService categoryCourseService;
    private InstructorService instructorService;
    private CourseService courseService;
    private LectureService lectureService;

    @Override
    public void init() throws ServletException {
        courseService = CourseService.getInstance();
        categoryCourseService = CategoryCourseService.getInstance();
        instructorService = InstructorService.getInstance();
        lectureService = LectureService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       String id = (String)request.getParameter("id");
        String page = (String) request.getParameter("page");
        String name = (String) request.getParameter("name");
        
        lectureService.deleteLecture(id);

        List<Lecture> listLecture = lectureService.getNextLectureAfterFilterAll(name, 0, page, 9);
        request.setAttribute("listLecture", listLecture);

        request.setAttribute("numberOfPage", lectureService.getNumberOfPageAfterFilterAll(name, 9));

        request.getRequestDispatcher("update-lecture.jsp").forward(request, response);  
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
