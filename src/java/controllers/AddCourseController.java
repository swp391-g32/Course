/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import entities.CategoryCourse;
import entities.Instructor;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import services.CategoryCourseService;
import services.CourseService;
import services.InstructorService;

/**
 *
 * @author FPT SHOP
 */
@WebServlet(name = "AddCourseController", urlPatterns = {"/instructor/add-course"})
public class AddCourseController extends HttpServlet {

    private CategoryCourseService categoryCourseService;
    private InstructorService instructorService;
    private CourseService courseService;

    @Override
    public void init() throws ServletException {
        courseService = CourseService.getInstance();
        categoryCourseService = CategoryCourseService.getInstance();
        instructorService = InstructorService.getInstance();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<CategoryCourse> listCategoryCourse = categoryCourseService.getAllCategoryCourse();
        request.setAttribute("listCategoryCourse", listCategoryCourse);

        List<Instructor> listInstructor = instructorService.getAllInstructor();
        request.setAttribute("listInstructor", listInstructor);

        request.getRequestDispatcher("add-course.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = (String) request.getParameter("name");
        String categoryId = (String) request.getParameter("categoryId");
        String startDate = (String) request.getParameter("startDate");
        String endDate = (String) request.getParameter("endDate");
        String description = (String) request.getParameter("description");
        String instructorId = (String) request.getParameter("instructorId");
        String price = (String) request.getParameter("price");
        String discount = (String) request.getParameter("discount");
        String img = (String) request.getParameter("img");
        String linkMeet = (String) request.getParameter("linkMeet");

        String errorTitle = courseService.sendErrorAddTitle(name);
        String errorPrice = courseService.sendErrorAddPrice(price);
        String errorDiscount = courseService.sendErrorAddDiscount(discount);
        String errorDateScope = courseService.sendErrorAddDateScope(startDate, endDate);
        String errorImg = courseService.sendErrorAddImg(img);
        String errorDescription = courseService.sendErrorAddDescription(description);

        if (errorTitle == null && errorPrice == null && errorDiscount == null && errorDateScope == null && errorImg == null && errorDescription == null) {
            courseService.insertNewCourse(name, img, startDate, endDate, description, categoryId, instructorId, price, discount, "");
            request.setAttribute("buildSuccess", "Add course successfully!");
        }

        if (errorTitle != null) {
            request.setAttribute("errorTitle", errorTitle);
        }
        
        if (errorPrice != null) {
            request.setAttribute("errorPrice", errorPrice);
        }
        
        if (errorDiscount != null) {
            request.setAttribute("errorDiscount", errorDiscount);
        }
        
        if (errorDateScope != null) {
            request.setAttribute("errorDateScope", errorDateScope);
        }
        
        if (errorImg != null) {
            request.setAttribute("errorImg", errorImg);
        }
        
        if (errorDescription != null) {
            request.setAttribute("errorDescription", errorDescription);
        }
        
        List<CategoryCourse> listCategoryCourse = categoryCourseService.getAllCategoryCourse();
        request.setAttribute("listCategoryCourse", listCategoryCourse);

        List<Instructor> listInstructor = instructorService.getAllInstructor();
        request.setAttribute("listInstructor", listInstructor);

        request.getRequestDispatcher("add-course.jsp").forward(request, response);
    }
}
