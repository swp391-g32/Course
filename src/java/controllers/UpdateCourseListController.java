/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import entities.CategoryCourse;
import entities.Course;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import services.CategoryCourseService;
import services.CourseService;

/**
 *
 * @author FPT SHOP
 */
@WebServlet(name = "UpdateCourseListController", urlPatterns = {"/instructor/update-course"})
public class UpdateCourseListController extends HttpServlet {

    private CategoryCourseService categoryCourseService;
    private CourseService courseService;

    @Override
    public void init() throws ServletException {
        courseService = CourseService.getInstance();
        categoryCourseService = CategoryCourseService.getInstance();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page = (String) request.getParameter("page");
        String name = (String) request.getParameter("name");
        String categoryId = (String) request.getParameter("categoryId");
        String startPrice = (String) request.getParameter("minValue");
        String endPrice = (String) request.getParameter("maxValue");
        String noc = (String) request.getParameter("noc");

        List<Course> listCourse = courseService.getNextCourseAfterFilterAll(categoryId, name, startPrice, endPrice, 0, page, 9);
        request.setAttribute("listCourse", listCourse);

        request.setAttribute("numberOfPage", courseService.getNumberOfPageAfterFilterAll(categoryId, name, startPrice, endPrice, 9));

        List<CategoryCourse> listCategoryCourse = categoryCourseService.getAllCategoryCourse();
        request.setAttribute("listCategoryCourse", listCategoryCourse);

        request.getRequestDispatcher("update-course.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page = (String) request.getParameter("page");
        String name = (String) request.getParameter("name");
        String categoryId = (String) request.getParameter("categoryId");
        String startPrice = (String) request.getParameter("minValue");
        String endPrice = (String) request.getParameter("maxValue");

        List<Course> listCourse = courseService.getNextCourseAfterFilterAll(categoryId, name, startPrice, endPrice, 0, page, 9);
        
        request.setAttribute("listCourse", listCourse);

        request.setAttribute("numberOfPage", courseService.getNumberOfPageAfterFilterAll(categoryId, name, startPrice, endPrice, 9));

        List<CategoryCourse> listCategoryCourse = categoryCourseService.getAllCategoryCourse();
        request.setAttribute("listCategoryCourse", listCategoryCourse);

        request.getRequestDispatcher("update-course.jsp").forward(request, response);

    }

}
