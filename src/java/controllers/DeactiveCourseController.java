/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import entities.CategoryCourse;
import entities.Course;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import services.CategoryCourseService;
import services.CourseService;

/**
 *
 * @author FPT SHOP
 */
@WebServlet(name = "DeleteCourseController", urlPatterns = {"/instructor/lecturer-delete-course"})
public class DeactiveCourseController extends HttpServlet {

    private CategoryCourseService categoryCourseService;
    private CourseService courseService;

    @Override
    public void init() throws ServletException {
        courseService = CourseService.getInstance();
        categoryCourseService = CategoryCourseService.getInstance();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String id = (String)request.getParameter("id");
        String page = (String) request.getParameter("page");
        String name = (String) request.getParameter("name");
        String categoryId = (String) request.getParameter("categoryId");
        String startPrice = (String) request.getParameter("minValue");
        String endPrice = (String) request.getParameter("maxValue");                 
        
        if(request.getParameter("id")==null){
            id = "";
        }
        
        if(request.getParameter("page")==null){
            page = "";
        }
        
        if(request.getParameter("name")==null){
            name = "";
        }
        
        if(request.getParameter("categoryId")==null){
            categoryId = "";
        }
        
        if(request.getParameter("minValue")==null){
            startPrice = "";
        }
        
        if(request.getParameter("maxValue")==null){
            endPrice = "";
        }
        
        courseService.deactiveCourse(id);
        
        response.sendRedirect("update-course?page=" + page + "&&name=" + name + "&&categoryId=" + categoryId + "&&minValue=" + startPrice + "&&maxValue=" + endPrice);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
