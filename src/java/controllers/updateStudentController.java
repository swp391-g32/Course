package controllers;

import daos.StudentDAO;
import entities.Student;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.regex.Pattern;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import jakarta.servlet.annotation.MultipartConfig;
import java.io.PrintWriter;

@WebServlet(name = "updateStudentController", urlPatterns = {"/admin/updateStudent"})
@MultipartConfig
public class updateStudentController extends HttpServlet {

    @Override
    public void init() throws ServletException {
        // Tạo thư mục 'images' khi khởi động ứng dụng nếu nó không tồn tại
        String appPath = getServletContext().getRealPath("");
        String uploadPath = appPath + File.separator + "images";
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet updateStudentController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet updateStudentController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("getId()"));
        StudentDAO dao = new StudentDAO();
        try {
            Student s = dao.getStudentById(id);
            request.setAttribute("student", s);
            request.getRequestDispatcher("updateStudent.jsp").forward(request, response);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        int student_id = Integer.parseInt(request.getParameter("id"));
        String email = request.getParameter("email");
        boolean email_verified = Boolean.parseBoolean(request.getParameter("emailVerified"));
        String phone_number = request.getParameter("phoneNumber");
        boolean active = Boolean.parseBoolean(request.getParameter("active"));
        String password = request.getParameter("password");
        String full_name = request.getParameter("fullName");
        String date_of_birth = request.getParameter("dateOfBirth");
        String family_name = request.getParameter("familyName");
        String given_name = request.getParameter("givenName");
        String enrollment_date = request.getParameter("enrollmentDate");
        int role_id = Integer.parseInt(request.getParameter("roleId"));

        Pattern emailPattern = Pattern.compile("^[\\w\\.-]+@[\\w\\.-]+\\.[a-z]{2,}$");
        Pattern phonePattern = Pattern.compile("^\\+?[0-9. ()-]{7,}$");

        boolean hasErrors = false;

        if (!emailPattern.matcher(email).matches()) {
            request.setAttribute("emailError", "Invalid email format.");
            hasErrors = true;
        }

        if (!phonePattern.matcher(phone_number).matches()) {
            request.setAttribute("phoneError", "Invalid phone number format.");
            hasErrors = true;
        }

        String errorPictureUrl = validateImage(request);
        if (errorPictureUrl != null) {
            request.setAttribute("pictureError", errorPictureUrl);
            hasErrors = true;
        }

        if (hasErrors) {
            request.setAttribute("student_id", student_id);
            request.setAttribute("email", email);
            request.setAttribute("email_verified", email_verified);
            request.setAttribute("phone_number", phone_number);
            request.setAttribute("active", active);
            request.setAttribute("password", password);
            request.setAttribute("full_name", full_name);
            request.setAttribute("date_of_birth", date_of_birth);
            request.setAttribute("family_name", family_name);
            request.setAttribute("given_name", given_name);
            request.setAttribute("enrollment_date", enrollment_date);
            request.setAttribute("role_id", role_id);
            request.setAttribute("errorPictureUrl", errorPictureUrl);
            request.getRequestDispatcher("updateStudent.jsp").forward(request, response);
            return;
        }

        StudentDAO dao = new StudentDAO();
        try {
            // Kiểm tra xem có tệp ảnh mới được tải lên hay không
            String fileName = handleFileUpload(request);
            if (fileName == null || fileName.isEmpty()) {
                // Không có tệp mới, giữ lại ảnh cũ
                Student existingStudent = dao.getStudentById(student_id);
                fileName = existingStudent.getPictureUrl();
            }

            Student sNew = new Student(student_id, email, email_verified, phone_number, active, password, full_name, date_of_birth, fileName, family_name, given_name, enrollment_date, role_id);
            dao.updateStudent(sNew);
            response.sendRedirect("searchbyname");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private String validateImage(HttpServletRequest request) {
        try {
            Part filePart = request.getPart("pictureUrl");
            if (filePart != null && filePart.getSize() > 0) {
                String contentType = filePart.getContentType();
                if (!contentType.startsWith("image/")) {
                    return "Invalid image format. Please upload a valid image file.";
                }
            }
        } catch (IOException | ServletException e) {
            return "Failed to process image upload.";
        }
        return null;
    }

    private String handleFileUpload(HttpServletRequest request) throws IOException, ServletException {
        Part filePart = request.getPart("pictureUrl");
        if (filePart != null && filePart.getSize() > 0) {
            // Validate the image
            String validationError = validateImage(request);
            if (validationError != null) {
                return null;
            }

            String appPath = request.getServletContext().getRealPath("");
            String uploadPath = appPath + File.separator + "images";
            String fileName = extractFileName(filePart);
            String filePath = uploadPath + File.separator + fileName;
            filePart.write(filePath);
            return fileName;
        }
        return null;
    }

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
