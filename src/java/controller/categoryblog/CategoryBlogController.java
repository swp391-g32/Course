/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.categoryblog;


import entities.Blog;
import entities.CategoryBlog;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import services.BlogService;
import services.CategoryBlogService;

/**
 *
 * @author khanh
 */
public class CategoryBlogController extends HttpServlet {

    private CategoryBlogService categoryBlogService;
    private BlogService blogService;

    @Override
    public void init() throws ServletException {
        blogService = BlogService.getInstance();
        categoryBlogService = CategoryBlogService.getInstance();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CategoryBlogController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CategoryBlogController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Blog> listBlog = blogService.getAllBlog();
//        List<Blog> listBlog = blogService.getAllBlog();
        System.out.println(listBlog.get(0).getTitle());
        request.setAttribute("listBlog", listBlog);
        List<CategoryBlog> listCategoryBlog = categoryBlogService.getAllCategoryBlog();
//        List<Blog> listBlog = blogService.getAllBlog();
        
        request.setAttribute("listCategoryBlog", listCategoryBlog);

//        List<Blog> listTwoRecentBlog = blogService.get2RecentBlog();
//        request.setAttribute("listRecentBlog", listTwoRecentBlog);
//        
//        request.setAttribute("numberOfPage", blogService.getNumberOfPageAfterFilterAll(categoryId, name));
//        
//        List<CategoryBlog> listCategoryBlog = categoryBlogService.getAllCategoryBlog();
//        request.setAttribute("listCategoryBlog", listCategoryBlog);
        request.getRequestDispatcher("Blog.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
